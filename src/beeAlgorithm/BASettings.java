package beeAlgorithm;

public class BASettings {
	public int n; // liczba pszcz�
	public int m; //liczba wybranych miejsc spo�r�d n odwiedzonych				//war:  m >= e
	public int e; // liczba najlepszych miejsc spo�r�d m wybranych
	public int nep; // liczba pszcz� rekrutowanych do e najlepszych miejsc		
	public int nsp; // liczba pszcz� rekrutowana do m-e gorszych miejsc
	public double ngh; // rozmiar s�siedztwa w %
	public int numberOfIterations; 												//war: numberOfIterations >=1
	public int saveEvery; // co ile iteracji ma by� zapisany najlepszy obecny rezultat
	public int mMin; // dolne ograniczenie ilosci kanalow
	public int mMax; // gorne          -||-
	public double lambda;
	public double mi;
	public boolean searchMax;
	
	
	public BASettings(){  // ustawienia domy�lne
		n = 100;
		m = 10;
		e = 3;
		nep = 40;
		nsp = 20;
		ngh = 0.1;
		numberOfIterations = 500;
		saveEvery = 10;
		lambda = 3;
		mi = 2;
		mMin = 1;
		mMax = 10;
		searchMax = true;
	}
}
