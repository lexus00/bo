package beeAlgorithm;

import main.Result;
import targetFunctions.TargetFunction;
import java.util.Random;

public class Bee{
	Result values;
	private Random rand;
	private int data;
	private final BASettings settings;
	private final TargetFunction fun;
	private int neighbourhoodSize;

	public Bee(BASettings settings, TargetFunction fun) {
		values = new Result();
		rand = new Random();
		this.settings = settings;
		this.fun = fun;
		neighbourhoodSize = (int) ((settings.mMax - settings.mMin) * settings.ngh);
	}
	public Result getValues(){
		return values;
	}
	public int getPlace(){
		return data;
	}
	public void findNewSite(){
		data = rand.nextInt(settings.mMax - settings.mMin + 1) + settings.mMin;
		values = ratePlace(data);
	}
	
	public void checkSite(int m){	// sprawdza s�siedztwo danego punktu
		if(m<0) return;
		if ( (m + neighbourhoodSize <= settings.mMax) &&  (m - neighbourhoodSize >= settings.mMin))
			data = m + rand.nextInt(neighbourhoodSize*2 + 1) - neighbourhoodSize;
		else if (m + neighbourhoodSize > settings.mMax) {
			int begin = m - neighbourhoodSize;
			if(begin < settings.mMin) begin = settings.mMin;
			int diff = settings.mMax - begin;
			data = rand.nextInt(diff + 1) + begin;
		} else {
			int end = m + neighbourhoodSize;
			if(end > settings.mMax) end = settings.mMax;
			int diff = end - settings.mMin;
			data = rand.nextInt(diff + 1) + settings.mMin;
		}
		
		if(data == m){
			if(settings.searchMax)
				values = new Result(-1,-2e200,0,0);
			else
				values = new Result(-1,2e200,0,0);
		}
		else{
			values = ratePlace(data);
		}
	}
	
	public Result ratePlace(int m){ // przyjmuje liczbe kanalow obs�ugi ...
        // trzeba to chyba jakoś uogólnić na system z kolejką o rozmiarze N
		return fun.calculate(m);
	}
}