package beeAlgorithm;

import java.util.Comparator;


public class BeeComparator implements Comparator<Bee>{
	private int searchMax;
	public BeeComparator(boolean max){
		if(max)
			searchMax = 1;
		else
			searchMax = -1;
	}
	@Override
	public int compare(Bee b1, Bee b2) {
		if( b1.values.fValue < b2.values.fValue)
			return 1 * searchMax;
		if( b1.values.fValue == b2.values.fValue)
			return 0;
		return -1 * searchMax;
	}
}
