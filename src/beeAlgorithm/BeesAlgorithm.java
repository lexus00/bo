package beeAlgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import main.IAlgorithm;
import targetFunctions.TargetFunction;
import main.Result;

public class BeesAlgorithm extends Thread implements IAlgorithm{
	private Bee bees[];
	private Result maxProfit;
	private ArrayList<Result> profits; // zyski w poszczegolnych iteracjach
	private final BASettings settings;
	private boolean finished;
	private boolean cancel;
	private ArrayList<Result> bestSites;
	private Random rand;
	private int bestResiltInIteration;
	private BeeComparator beeComparator;
	private ResultComparator resultComparator;
	
	public BeesAlgorithm(BASettings settings, TargetFunction fun){
		this.settings = settings;
		beeComparator = new BeeComparator( settings.searchMax );
		resultComparator = new ResultComparator( settings.searchMax );
		bestResiltInIteration = 0;
		finished = false;
		cancel = false;
		rand = new Random();
		profits = new ArrayList<Result>();
		bestSites = new ArrayList<Result>();
		maxProfit = new Result();
		bees = new Bee[settings.n];
		for(int i = 0; i<settings.n; i++)
			bees[i] = new Bee(settings,fun);
	}
	public Result getMaxProfit(){
		return maxProfit;
	}
	public ArrayList<Result> getProfits(){
		return profits;
	}
	public boolean isFinished(){
		return finished;
	}
	public void cancel(){
		cancel = true;
	}
	public int getIterationWhenFindBestResult(){
		return bestResiltInIteration;
	}
	
	public void run(){
	//	System.out.println("searchMax " + settings.searchMax);
		int j;
		//pierwsza iteracja algorytmu
		for(int i=0; i<settings.n; i++){
			bees[i].findNewSite();
		}
		Arrays.sort(bees, beeComparator);
		for(int i=0; i<settings.m; i++){		// zapamietanie najlepszych miejsc
			bestSites.add(bees[i].values);
		}
		maxProfit = bestSites.get(0);
		profits.add(maxProfit);
		// w�a�ciwa cz�� algorytmu
		for(int i=1; i<settings.numberOfIterations; i++){
			//pszcz�ki do najlepszych e miejsc
			for(j=0; j<settings.nep; j++){
				bees[j].checkSite( bestSites.get(rand.nextInt(settings.e)).m );
			}
			
			//pszcz�ki do najlepszych m-e gorszych miejsc
			for(; j<settings.nsp + settings.nep; j++ ){
				bees[j].checkSite( bestSites.get(rand.nextInt(settings.m - settings.e) + settings.e ).m );
			}
			
			//pszcz�ki do randomowych miejsc
			for(; j<settings.n; j++){
				bees[j].findNewSite();
			}
			
			Arrays.sort(bees, beeComparator);
		//	for(int h=0;h<bees.length; h++)
		//		System.out.print(bees[h].getValues().fValue+" ");
		//	System.out.println();
			int k=0;
			while(k < bees.length && resultComparator.compare(bees[k].values, bestSites.get(settings.m - 1))  < 0   ){  // aktualizacja wynikow w tablicy najlepszych miejsc
				bestSites.add(bees[k].values);
				Collections.sort(bestSites, resultComparator);		
				bestSites.remove(settings.m);
				k++;
		//		for(int h=0;h<bestSites.size(); h++)
		//			System.out.print(bestSites.get(h).fValue+" ");
		//		System.out.println();
			}
			
			// zapisanie najlepszego wyniku z iteracji
			if (resultComparator.compare(bestSites.get(0),maxProfit) <0 ){
				bestResiltInIteration = i;
				maxProfit = bestSites.get(0);
			}
			
	//		System.out.println("max "+maxProfit.fValue);
			
			if( i%settings.saveEvery == 0)
				profits.add(maxProfit);
			
			assert bestSites.size() != settings.m : "Houston mamy problem";
			if(cancel) break;
		}
		finished = true;
	}
	
	
}
