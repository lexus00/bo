package beeAlgorithm;

import java.util.Comparator;

import main.Result;

public class ResultComparator implements Comparator<Result>{
	private int searchMax;
	public ResultComparator(boolean max){
		if(max)
			searchMax = 1;
		else
			searchMax = -1;
	}
	
	@Override
	public int compare(Result o1, Result o2) {
		if( o1.fValue < o2.fValue)
			return 1 * searchMax;
		if( o1.fValue == o2.fValue)
			return 0;
		return -1 * searchMax;
	}
}
