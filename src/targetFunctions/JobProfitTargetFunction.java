package targetFunctions;

import main.Result;
import queues.Queue;

public class JobProfitTargetFunction implements TargetFunction {

    private double jobProfit = 1;
    private double costOfServerDepreciation = 1;
    private Queue queue;

    public JobProfitTargetFunction(Queue queue) {
        this.queue = queue;
    }

    public void setJobProfit(double jobProfit) {
        this.jobProfit = jobProfit;
    }

    public void setCostOfServerDepreciation(double costOfServerDepreciation) {
        this.costOfServerDepreciation = costOfServerDepreciation;
    }

    @Override
    public Result calculate(int m) {
        queue.setM(m);

        double lambda = queue.getLambda();
        int n = queue.getN();

        double probabilityOfRejection = queue.probabilityOfRejection();
        double averageSystemSize = queue.averageSystemSize();
        double functionValue = jobProfit*lambda*(1-probabilityOfRejection) - costOfServerDepreciation * (m + n);

        return new Result(m, functionValue, probabilityOfRejection, averageSystemSize);
    }

    @Override
    public String toString() {
        return String.format("%f*lambda*(1-probabilityOfRejection) - %f * (m + N)", jobProfit, costOfServerDepreciation);
    }
}
