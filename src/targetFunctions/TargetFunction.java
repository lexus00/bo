package targetFunctions;

import main.Result;
import queues.Queue;

public interface TargetFunction {
	public Result calculate(int m);
}
