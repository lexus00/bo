package targetFunctions;

import main.MathUtils;
import main.Result;
import queues.Queue;

public class JobCostTargetFunction implements TargetFunction {
    private double jobCost = 4.0;
    private double costOfServerDepreciation = 1.0;
    private Queue queue;

    public JobCostTargetFunction(Queue queue) {
        this.queue = queue;
    }

    public void setJobCost(double jobCost) {
        this.jobCost = jobCost;
    }

    public void setCostOfServerDepreciation(double costOfServerDepreciation) {
        this.costOfServerDepreciation = costOfServerDepreciation;
    }

    @Override
    public Result calculate(int m) {
        queue.setM(m);

        double lambda = queue.getLambda();
        double mi = queue.getMi();

        double probabilityOfRejection = queue.probabilityOfRejection();
        double averageSystemSize = queue.averageSystemSize();
        double functionValue = jobCost*lambda/mi *
                                (1-Math.pow(lambda/mi, m)/(MathUtils.factorial(m)*MathUtils.sum_of_series(0, m, lambda/mi)))
                                - m * costOfServerDepreciation;

        return new Result(m, functionValue, probabilityOfRejection, averageSystemSize);
    }

    @Override
    public String toString() {
        return String.format("jobCost = %f, costOfServerDepreciation = %f", jobCost, costOfServerDepreciation);
    }
}
