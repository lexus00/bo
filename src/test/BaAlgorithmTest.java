package test;

import static org.junit.Assert.assertTrue;
import main.IAlgorithm;
import main.Result;

import org.junit.Before;
import org.junit.Test;

import queues.Queue;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;
import targetFunctions.JobProfitTargetFunction;
import targetFunctions.TMPFunction;
import targetFunctions.TargetFunction;
import beeAlgorithm.BASettings;
import beeAlgorithm.Bee;
import beeAlgorithm.BeeComparator;
import beeAlgorithm.BeesAlgorithm;
import beeAlgorithm.ResultComparator;

public class BaAlgorithmTest {
	private BASettings baSet;
	
	@Before
	public void setUp(){
		baSet = new BASettings();
		baSet.n = 50;
		baSet.m = 10;
		baSet.e = 3;
		baSet.nep = 20;
		baSet.nsp = 10;
		baSet.ngh = 0.2;
		baSet.numberOfIterations = 50;
		baSet.saveEvery = 10;
		baSet.lambda = 3;
		baSet.mi = 2;
		baSet.mMin = 1;
		baSet.mMax = 50;
		baSet.searchMax = true;
	}
	
	@Test
	public void testFromDoc() {
		// M/M/m/Fifo/m+N system
		//FixedSizeQueue(int m, int n, double lambda, double mi)
		// M/M/m/-/m system
		//QueueWithLosses(int m, double lambda, double mi)
		int testTableSize = 12; 
		Queue queues[] ={
				new QueueWithLosses(2, 1, 1),   // A
				new QueueWithLosses(2, 10, 5),  // B
				new QueueWithLosses(2, 10, 10), // C
				new QueueWithLosses(2, 10, 15), // D
				new QueueWithLosses(2, 25, 5),  // E
				new QueueWithLosses(2, 50, 5),   // F
		};
		TargetFunction functionTable[] = new TargetFunction[testTableSize];
		boolean searchMaxTable[] = new boolean[testTableSize];
		
		for(int i=0; i<testTableSize/2;i++){
			functionTable[i] = new JobCostTargetFunction(queues[i]);
			searchMaxTable[i] = true;
		}
		for(int i=testTableSize/2; i<testTableSize;i++){
			functionTable[i] = new JobProfitTargetFunction(queues[i- testTableSize/2]);
			searchMaxTable[i] = true;
		}
	//	System.out.println("fv = "+functionTable[6].calculate(2).fValue);
	//	System.out.println("fv = "+functionTable[6].calculate(1).fValue);
		
		int mMinTable[] 				= {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		int mMaxTable[] 				= {50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50};
		double resultTable[]			= {1.2, 3.31, 1.20, 0.6, 10.599, 23.727, 1.2, 3.31, 1.20, 0.6, 10.599, 23.727};	
		double posibleDiffFromResult[] 	= {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}; // bezwzgledna odleglosc od wyniku
		for(int i=0; i<testTableSize; i++){
			baSet.mMin = mMinTable[i]; 
			baSet.mMax = mMaxTable[i]; 
			baSet.searchMax = searchMaxTable[i];
			double result[] = simpleTest(baSet, functionTable[i]); 
			if(searchMaxTable[i]){
				assertTrue("\ni("+i+") expexted "+ resultTable[i] +" but get "+result[1]+" for m ="+result[0], resultTable[i] - result[1] <= posibleDiffFromResult[i]);
				if(resultTable[i] < result[1]) System.out.println("i("+i+") find better result than "+resultTable[i]+" for m = "+ result[0]+" f(m) = "+result[1]+" in iteration "+result[2] );
			}else{
				assertTrue("\ni("+i+") expexted "+ resultTable[i] +" but get "+result[1]+" for m ="+result[0], result[1] - resultTable[i] <= posibleDiffFromResult[i]);
				if(resultTable[i] > result[1]) System.out.println("i("+i+") find better result than "+resultTable[i]+" for m = "+ result[0]+" f(m) = "+result[1]+" in iteration "+result[2] );
			}
			//assertTrue("\ni("+i+") expexted "+ resultTable[i] +" but get "+result[1]+" for m ="+result[0], Math.abs(resultTable[i] - result[1]) <= posibleDiffFromResult[i]);
		}
	}
	@Test
	public void test(){	
		int mMinTable[] 				= {1,1};
		int mMaxTable[] 				= {50,50};
		double resultTable[]			= {50,50};
		boolean searchMaxTable[]		= {true,true};
		double posibleDiffFromResult[] 	= {0,0}; // bezwzgledna odleglosc od wyniku
		TargetFunction functionTable[]  = {new TMPFunction(),new TMPFunction()};  
		
		for(int i=0; i<mMaxTable.length; i++){
			baSet.mMin = mMinTable[i]; 
			baSet.mMax = mMaxTable[i]; 
			baSet.searchMax = searchMaxTable[i];
			double result[] = simpleTest(baSet, functionTable[i]); 
			assertTrue("\nexpexted "+ resultTable[i] +" but get "+result[1]+" for m ="+result[0], Math.abs(resultTable[i] - result[1]) <= posibleDiffFromResult[i]);
		}
	}
	
	@Test
	public void resultComparatorTest() {
		Result r1 = new Result(1,1.1,0,0);
		Result r2 = new Result(-1,1,0,0);
		ResultComparator resultComparator = new ResultComparator(true);
		assertTrue(resultComparator.compare(r1,r2) < 0 );
		assertTrue(resultComparator.compare(r2,r1) > 0 );
		assertTrue(resultComparator.compare(r1,r1) == 0 );
	}
	@Test
	public void beeComparatorTest() {
		Bee b1 = new Bee(baSet,new TMPFunction());
		Bee b2 = new Bee(baSet,new TMPFunction());
		BeeComparator beeComparator = new BeeComparator(true);
		b1.findNewSite();
		b2.findNewSite();
		if(b1.getValues().fValue > b2.getValues().fValue){
			assertTrue(beeComparator.compare(b1,b2) < 0 );
			assertTrue(beeComparator.compare(b2,b1) > 0 );
			assertTrue(beeComparator.compare(b1,b1) == 0 );
		}else if (b1.getValues().fValue < b2.getValues().fValue){
			assertTrue(beeComparator.compare(b1,b2) > 0 );
			assertTrue(beeComparator.compare(b2,b1) < 0 );
			assertTrue(beeComparator.compare(b1,b1) == 0 );
		}else
			assertTrue(beeComparator.compare(b1,b2) == 0 );
	}
	
	@Test
	public void beeTest(){
		Bee bee = new Bee(baSet,new TMPFunction());
		int nrOfTest = 50;
		int counterDiff = 0;
		double acceptFrom = 0.5;
		
		int m = baSet.mMin;
		for(int i=0; i<nrOfTest; i++){
			bee.checkSite(m);
			assertTrue("bee check place lower than mMin bee.m ="+bee.getPlace(),bee.getPlace()>=baSet.mMin);
		}
		
		m = baSet.mMax;
		for(int i=0; i<nrOfTest; i++){
			bee.checkSite(m);
			assertTrue("bee check place greater than mMax bee.m ="+bee.getPlace(),bee.getPlace()<=baSet.mMax);
		}
		
		m = (baSet.mMax + baSet.mMin)/2;
		for(int i=0; i<nrOfTest; i++){
			bee.checkSite(m);
			if(bee.getPlace() != m) ++counterDiff;
		}	
		assertTrue("\nbee.checkSite random fails,\ndiffrent from m = "+counterDiff+ " acceptable from "+acceptFrom*nrOfTest,counterDiff>=(acceptFrom*nrOfTest) );
	}
	
	@Test
	public void basicTest(){
		baSet.mMin = 1;
		baSet.mMax = 50;
		baSet.searchMax = true;
		double result[] = simpleTest(baSet, new TMPFunction());
		assertTrue("mMax = "+baSet.mMax+" res = "+ result[1] +" it = "+result[2],baSet.mMax == result[1]);
		baSet.searchMax = false;
		result = simpleTest(baSet, new TMPFunction());
		assertTrue("mMin = "+baSet.mMin+" res = "+ result[1] +" it = "+result[2],baSet.mMin == result[1]);
	}
	
	private double[] simpleTest(BASettings baSet, TargetFunction func){
		IAlgorithm alg = new BeesAlgorithm(baSet , func);
		alg.run();
		double res[] = new double[3];
		res[0] = alg.getMaxProfit().m;
		res[1] = alg.getMaxProfit().fValue;
		res[2] = alg.getIterationWhenFindBestResult();
		return res;
	}
}
