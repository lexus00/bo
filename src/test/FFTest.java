package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import queues.*;
import targetFunctions.*;
import fireflyAlgorithm.*;
import main.IAlgorithm;
import main.Result;

public class FFTest {
	private FASettings faSet;
	
	@Before
	public void setUp(){
		faSet = new FASettings();
	}
	
	@Test
	public void test() {
		FixedSizeQueue queue = new FixedSizeQueue(10,10,25,5);
		JobProfitTargetFunction fun = new JobProfitTargetFunction(queue);
		FireflyAlgorithm fly = new FireflyAlgorithm(faSet,fun);
		fly.run();
	}
	
}
