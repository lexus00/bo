package queues;


import main.MathUtils;

// M/M/m/-/m system

public class QueueWithLosses implements Queue {
    private int m;
    private double lambda;
    private double mi;

    public QueueWithLosses(int m, double lambda, double mi) {
        this.m = m;
        this.lambda = lambda;
        this.mi = mi;
    }


    @Override
    public double probabilityOfRejection() {
        return (Math.pow(lambda/mi, m)/ MathUtils.factorial(m))/MathUtils.sum_of_series(0, m, lambda/mi);
    }

    @Override
    public double averageQueueSize() {
        return 0;
    }

    @Override
    public double averageSystemSize() {
        return (lambda/mi)*(1-probabilityOfRejection());
    }

    @Override
    public double averageQueueWaitingTime() {
        return 0;
    }

    @Override
    public double averageTimeInSystem() {
        return averageSystemSize()/lambda;
    }


    @Override
    public double getLambda() {
        return lambda;
    }

    @Override
    public double getMi() {
        return mi;
    }

    @Override
    public double getRo() {
        return lambda/mi;
    }

    @Override
    public int getM() {
        return m;
    }

    @Override
    public int getN() {
        // actually no queue here bot for sake of target function generality
        return 0;
    }

    @Override
    public void setM(int m) {
        this.m = m;
    }
}
