package queues;


import main.MathUtils;

// M/M/m/Fifo/m+N system

public class FixedSizeQueue implements Queue {
    private final double lambda;
    private final double mi;
    private int m;
    private int n;
    private double pi0;
    private double ro;

    public FixedSizeQueue(int m, int n, double lambda, double mi) {
        this.m = m;
        this.n = n;
        this.lambda = lambda;
        this.mi = mi;
        this.ro = lambda/mi;
        calculatePi0();
    }

    private void calculatePi0() {
        if(ro == m) {
            pi0 = Math.pow(MathUtils.sum_of_series(0, m - 1, ro) + Math.pow(ro, m)*(n+1)/ MathUtils.factorial(m), -1);
        } else {
            pi0 = Math.pow(MathUtils.sum_of_series(0, m-1, ro) + Math.pow(ro, m)*(n+1)*(1-Math.pow(ro/m, n+1)/(1-ro/m)), -1);
        }
    }

    @Override
    public double probabilityOfRejection() {
        return Math.pow(ro, m + n)*pi0/(Math.pow(m, n)* MathUtils.factorial(m));
    }

    @Override
    public double averageQueueSize() {
        return Math.pow(m, m)*n*(n+1)*pi0/(2* MathUtils.factorial(m));
    }

    @Override
    public double averageQueueWaitingTime() {
        return averageQueueSize()/lambda;
    }

    @Override
    public double averageSystemSize() {
        return averageQueueSize() + ro*(1-probabilityOfRejection());
    }

    @Override
    public double averageTimeInSystem() {
        return averageSystemSize()/lambda;
    }

    @Override
    public double getLambda() {
        return lambda;
    }

    @Override
    public double getMi() {
        return mi;
    }

    @Override
    public double getRo() {
        return lambda/mi;
    }

    @Override
    public int getM() {
        return m;
    }

    @Override
    public int getN() {
        return n;
    }

    @Override
    public void setM(int m) {
        this.m = m;
    }
}
