package queues;


public interface Queue {
    double probabilityOfRejection();

    double averageQueueSize(); // Q
    double averageQueueWaitingTime(); // W

    double averageSystemSize(); // K
    double averageTimeInSystem(); // T

    double getLambda();
    double getMi();
    double getRo();
    int getM();
    int getN();

    void setM(int m);
}
