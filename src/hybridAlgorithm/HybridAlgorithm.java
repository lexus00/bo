package hybridAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import main.IAlgorithm;
import targetFunctions.TargetFunction;
import main.Result;

public class HybridAlgorithm extends Thread implements IAlgorithm{
	private Result maxProfit;
	private ArrayList<Result> profits; // zyski w poszczegolnych iteracjach
	private final HASettings settings;
	private boolean finished;
	private boolean cancel;
	private ArrayList<Result> bestSites;
	private Random rand;
	private int bestResiltInIteration;
	private int[] bestPlaces;  // np. bestPlace[5] == 2 ozn. �e do 6(5) najleszego wyniku maja leciec 2 swietliki
	private int[] almostBestPlaces;// np. al(..)[5] == 2 ozn. �e do 6+settings.e najlepszego wyniku maja leciec 2 swietliki
	private ResultComparator resultComparator;

	
	private Firefly firefly;
	
	public HybridAlgorithm(HASettings settings, TargetFunction fun){
		bestPlaces = new int[settings.e];
		almostBestPlaces = new int[settings.m - settings.e];
		this.settings = settings;
		bestResiltInIteration = 0;
		finished = false;
		cancel = false;
		rand = new Random();
		profits = new ArrayList<Result>();
		bestSites = new ArrayList<Result>();
		maxProfit = new Result();
		resultComparator = new ResultComparator();
		
		firefly = new Firefly(fun,settings);
	}
	public Result getMaxProfit(){
		return maxProfit;
	}
	public ArrayList<Result> getProfits(){
		return profits;
	}
	public boolean isFinished(){
		return finished;
	}
	public void cancel(){
		cancel = true;
	}
	public int getIterationWhenFindBestResult(){
		return bestResiltInIteration;
	}
	
	public void run(){
		int j;
		for(int i=0;i<settings.m;i++)
			bestSites.add(new Result());
		ArrayList<Result> fireflyResult;
		int leftBoundary;
		int rightBoundary;
		/*
		//pierwsza iteracja algorytmu
		//wszystkie �wietliki  do losowych miejsc
		fireflyResult =  firefly.fly(settings.mMin, settings.mMax, settings.n);
		mergeSortRemoveRest(bestSites,fireflyResult,settings.m);
		*/
		//wlasciwa czesc algorytmu

		System.out.println("BOUNDARIES: " + settings.mMin + settings.mMax);

		for(int i=1; i<settings.numberOfIterations; i++){
			zeroTabs();
			//swietliki do e najlepszych miejsc
			for(j=0; j<settings.nep; j++){
				int r = rand.nextInt(settings.e);
				bestPlaces[r] += 1;
			}
			
		/*	for(int k=0;k<bestPlaces.length;k++)
				System.out.print(bestPlaces[k]+" ");
			System.out.println();*/
			
			for(int it=0; it<bestPlaces.length; it++){
				if(bestPlaces[it] == 0) continue;
			//	System.out.println(bestSites.get(it).m);
				leftBoundary = bestSites.get(it).m - settings.neighbourhoodRadius < settings.mMin ? settings.mMin : bestSites.get(it).m - settings.neighbourhoodRadius;
				rightBoundary = bestSites.get(it).m + settings.neighbourhoodRadius > settings.mMax ? settings.mMax : bestSites.get(it).m + settings.neighbourhoodRadius;
				fireflyResult = firefly.fly(leftBoundary, rightBoundary, bestPlaces[it]);
				mergeSortRemoveRest(bestSites,fireflyResult,settings.m);
			}
			
			
			//swietliki do m - e gorszych miejsc
			for(; j<settings.nsp + settings.nep; j++ ){
				int r = rand.nextInt(settings.m - settings.e); 
				almostBestPlaces[r] += 1; //!! przesuniecie w tablicy wynik�w o settings.e  
			}
			
			/*for(int k=0;k<almostBestPlaces.length;k++)
				System.out.print(almostBestPlaces[k]+" ");
			System.out.println();*/
			
			for(int it=0;it<almostBestPlaces.length;it++){
				if(almostBestPlaces[it] == 0) continue;
		//System.out.println(bestSites.get(it+settings.e).m);
				leftBoundary = bestSites.get(it+settings.e).m - settings.neighbourhoodRadius < settings.mMin ? settings.mMin : bestSites.get(it+settings.e).m - settings.neighbourhoodRadius;
				rightBoundary = bestSites.get(it+settings.e).m + settings.neighbourhoodRadius > settings.mMax ? settings.mMax : bestSites.get(it+settings.e).m + settings.neighbourhoodRadius;
				fireflyResult = firefly.fly(leftBoundary, rightBoundary, almostBestPlaces[it]);
				mergeSortRemoveRest(bestSites,fireflyResult,settings.m);
			}
			//swietliki do losowych miejsc
		//System.out.println(settings.n - j);
			fireflyResult =  firefly.fly(settings.mMin, settings.mMax, settings.n - j); // tutaj powinny zosta� zwr�cone wyniki Alg �wietlika
			mergeSortRemoveRest(bestSites,fireflyResult,settings.m);
			
			// zapisanie najlepszego wyniku z iteracji
			if (maxProfit.fValue < bestSites.get(0).fValue ){
				bestResiltInIteration = i;
				maxProfit = bestSites.get(0);
			}
			//if( i%settings.saveEvery == 0)
				profits.add(maxProfit);
					
			assert bestSites.size() != settings.m : "Houston mamy problem";
			if(cancel) break;
		}
		finished = true;
	}
	
	private void zeroTabs(){
		for(int i=0; i<bestPlaces.length; i++)
			bestPlaces[i] =  0;
		for(int i=0; i<almostBestPlaces.length; i++)
			almostBestPlaces[i]  = 0;
	}

	private void mergeSortRemoveRest(ArrayList<Result> ret, ArrayList<Result> other,int returnSize){
/*		for(int i=0; i<other.size();i++)
			System.out.print(other.get(i).m+ " ");
		System.out.println();
		for(int i=0; i<ret.size();i++)
			System.out.print(ret.get(i).m+ " ");
		System.out.println();*/
		check(other);
		ret.addAll(other);
		Collections.sort(ret,resultComparator);
/*		System.out.println("RET:");
		for(int i=0; i<ret.size();i++)
			System.out.print(ret.get(i).m+ " " +ret.get(i).fValue + ", ");
		System.out.println();*/
		while(ret.size() > returnSize ){
			ret.remove(returnSize);
		}
		
	/*	for(int i=0; i<ret.size();i++)
			System.out.print(ret.get(i).fValue+ " ");
		System.out.println(); */
	}
	private void check(ArrayList<Result> res){
		for(Result r : res){
			if(r.m>settings.mMax || r.m<settings.mMin){
				System.out.println("WYKRACZA POZA ZAKRES");
			}
		}
	}
}
