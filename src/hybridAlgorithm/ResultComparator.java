package hybridAlgorithm;

import main.Result;
import java.util.Comparator;


public class ResultComparator implements Comparator<Result> {

	@Override
	public int compare(Result o1, Result o2) {
		if( o1.fValue < o2.fValue)
			return 1;
		if( o1.fValue == o2.fValue)
			return 0;
		return -1;
	}
}