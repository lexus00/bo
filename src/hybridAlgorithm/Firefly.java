package hybridAlgorithm;

import java.util.ArrayList;

import targetFunctions.TargetFunction;
import fireflyAlgorithm.FASettings;
import fireflyAlgorithm.FireflyAlgorithm;
import main.Result;

public class Firefly {
	
	public TargetFunction fun;
	public HASettings hset;
	
	public Firefly(TargetFunction fun,HASettings hset){
		this.fun = fun;
		this.hset = hset;
	}

	
	public ArrayList<Result> fly(int bottom_boundary, int up_boundary, int num_of_fflies){
//		System.out.println("bot "+ bottom_boundary +" up "+ up_boundary+ " num " +num_of_fflies);
		//ArrayList<Result> result = new ArrayList<Result>();
		
		FASettings faSet = new FASettings(num_of_fflies, hset.numberOfIterationsForFireflies, hset.maxAttractiveness , hset.alfa, hset.lightAbsorbtion, bottom_boundary, up_boundary);
		
		FireflyAlgorithm fly = new FireflyAlgorithm(faSet,fun);
		fly.run();

	/*	System.out.println("max "+ fly.getMaxProfit().fValue);
		for(int i=0; i<fly.getProfits().size();i++)
			System.out.print(fly.getProfits().get(i).fValue+ " ");
		System.out.println();
		System.out.println("Current profits: ");
	    for ( int i = 0; i < fly.getProfits().size(); i++ )
			System.out.print(fly.getProfits().get(i).m + " ");
		System.out.println(); */
		return fly.getProfits();
	}
	
	
}
