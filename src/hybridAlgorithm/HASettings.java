package hybridAlgorithm;

public class HASettings{

	// pszczo�a := �wietlik
	public int n; // liczba pszcz�
	public int m; //liczba wybranych miejsc spo�r�d n odwiedzonych				//war:  m >= e
	public int e; // liczba najlepszych miejsc spo�r�d m wybranych
	public int nep; // liczba pszcz� rekrutowanych do e najlepszych miejsc		
	public int nsp; // liczba pszcz� rekrutowana do m-e gorszych miejsc
	public double ngh; // rozmiar s�siedztwa w %
	public int numberOfIterations; 												//war: numberOfIterations >=1
	public int saveEvery; // co ile iteracji ma by� zapisany najlepszy obecny rezultat
	public int mMin; // dolne ograniczenie ilosci kanalow
	public int mMax; // gorne          -||-
	public double lambda;
	public double mi;
	public boolean searchMax;
	public double lightAbsorbtion;  // Gamma
	public double maxAttractiveness;  // Beta0
	public double alfa; 
	public int numberOfIterationsForFireflies;
	public int neighbourhoodRadius;
	
	public HASettings(){  // ustawienia domy�lne
		n = 10;
		m = 10;
		e = 3;
		nep = 4;
		nsp = 2;
		ngh = 0.01;
		numberOfIterations = 10;
		saveEvery = 10;
		mMin = 1;
		mMax = 50;
		lambda = 3;
		mi = 2;
		maxAttractiveness = 0.9;
		alfa = 0.1;
		lightAbsorbtion = 1;
		numberOfIterationsForFireflies = 5;
		neighbourhoodRadius = 4;
	}
}

