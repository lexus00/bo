package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import queues.Queue;
import queues.QueueWithLosses;
import targetFunctions.JobProfitTargetFunction;

public class FifoTargetFunctionWindow {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public FifoTargetFunctionWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(final Settings settings) {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 202);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblPodajDane = new JLabel("Podaj dane:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPodajDane, 6, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblPodajDane, 6, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblPodajDane, 104, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblPodajDane);
		
		JLabel lblKosztKanaluObslufi = new JLabel("Koszt utrzymania kanalu obslugi:");
		springLayout.putConstraint(SpringLayout.NORTH, lblKosztKanaluObslufi, 54, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblKosztKanaluObslufi, 6, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblKosztKanaluObslufi);
		
		final JSpinner kosztKanaluObslugi = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.NORTH, kosztKanaluObslugi, -3, SpringLayout.NORTH, lblKosztKanaluObslufi);
		springLayout.putConstraint(SpringLayout.WEST, kosztKanaluObslugi, 54, SpringLayout.EAST, lblKosztKanaluObslufi);
		kosztKanaluObslugi.setModel(new SpinnerNumberModel(new Double(1), new Double(0), null, new Double(1)));
		frame.getContentPane().add(kosztKanaluObslugi);
		
		JLabel lblPrzychod = new JLabel("Przychod");
		springLayout.putConstraint(SpringLayout.NORTH, lblPrzychod, 80, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblPrzychod, 6, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblPrzychod, 104, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblPrzychod);
		
		final JSpinner przychod = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.NORTH, przychod, 6, SpringLayout.SOUTH, kosztKanaluObslugi);
		springLayout.putConstraint(SpringLayout.WEST, przychod, 0, SpringLayout.WEST, kosztKanaluObslugi);
		przychod.setModel(new SpinnerNumberModel(new Double(1), new Double(0), null, new Double(1)));
		frame.getContentPane().add(przychod);
		
		JButton btnDalej = new JButton("Dalej");
		springLayout.putConstraint(SpringLayout.SOUTH, btnDalej, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnDalej, -37, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(btnDalej);
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		    	SpinnerModel model = kosztKanaluObslugi.getModel();
		    	double koszt = (Double) model.getValue();
		    	model = przychod.getModel();
		    	double jobProfit = (Double)model.getValue();
		    	int m =settings.getQueue().getM();
		    	double lambda = settings.getQueue().getLambda();
		    	double mi=settings.getQueue().getMi();
		    	Queue q = new QueueWithLosses(m, lambda, mi);
		    	JobProfitTargetFunction jobProfitTargetFunction = new JobProfitTargetFunction(q);
		    	jobProfitTargetFunction.setCostOfServerDepreciation(koszt);
		    	jobProfitTargetFunction.setJobProfit(jobProfit);
		    	settings.setTargetFunction(jobProfitTargetFunction);
		    	if(settings.getBee()!=null) new BeesWindow(settings);
		    	else if(settings.getFirefly()!=null) new FireFlyWindow(settings);
		    	else if(settings.getHybrid()!=null) new HybridWindow(settings);
		       frame.setVisible(false);
		    }
		});
		frame.setVisible(true);
	}

}
