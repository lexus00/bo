package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;

public class MainWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Wybor systemu do optymalizacji");
		frame.setBounds(100, 100, 450, 202);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JButton btnDalej = new JButton("Dalej");
		springLayout.putConstraint(SpringLayout.NORTH, btnDalej, -33, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnDalej, 327, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnDalej, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnDalej, -39, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(btnDalej);
		
		JLabel lblKtorySystemChcesz = new JLabel("Ktory system chcesz optymalizowac?");
		springLayout.putConstraint(SpringLayout.NORTH, lblKtorySystemChcesz, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblKtorySystemChcesz, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblKtorySystemChcesz);
		
		final JRadioButton notFifoRadioButton = new JRadioButton("M|M|m|-|m");
		springLayout.putConstraint(SpringLayout.WEST, notFifoRadioButton, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(notFifoRadioButton);
		
		final JRadioButton fifoRadioButton = new JRadioButton("M|M|m|FIFO|m+N");
		springLayout.putConstraint(SpringLayout.NORTH, fifoRadioButton, 75, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, notFifoRadioButton, -6, SpringLayout.NORTH, fifoRadioButton);
		springLayout.putConstraint(SpringLayout.WEST, fifoRadioButton, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(fifoRadioButton);
		
		ButtonGroup group = new ButtonGroup();
		group.add(notFifoRadioButton);
		group.add(fifoRadioButton);
		
		final JLabel label = new JLabel("\"\"");
		label.setForeground(Color.RED);
		label.setText("Musisz wybrac przynajmniej jeden system");
		label.setVisible(false);
		
		springLayout.putConstraint(SpringLayout.NORTH, label, 6, SpringLayout.SOUTH, fifoRadioButton);
		springLayout.putConstraint(SpringLayout.WEST, label, 0, SpringLayout.WEST, lblKtorySystemChcesz);
		frame.getContentPane().add(label);
		
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		    	if((!fifoRadioButton.isSelected()&& !notFifoRadioButton.isSelected())) {
		    		label.setVisible(true);
		    	}
		    	else {
			    	Settings settings = new Settings();
			       	if(fifoRadioButton.isSelected()) {
			       		settings.setFifo(true);			    		
			    	}else settings.setFifo(false);
			       	new AlgorithmWindow(settings);
			       frame.setVisible(false);
		    	}
		    }
		});
	}
}
