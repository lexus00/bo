package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import queues.Queue;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;

public class TargetFunctionWindow {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public TargetFunctionWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(final Settings settings) {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 202);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblPodajDaneFunkcji = new JLabel("Podaj dane funkcji celu:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPodajDaneFunkcji, 6, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblPodajDaneFunkcji, 6, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblPodajDaneFunkcji);
		
		JLabel lblKosztKanaluObslugi = new JLabel("Amortyzacja kanalu obslugi:");
		springLayout.putConstraint(SpringLayout.NORTH, lblKosztKanaluObslugi, 54, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblKosztKanaluObslugi, 6, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblKosztKanaluObslugi, 228, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblKosztKanaluObslugi);
		
		final JSpinner kosztKanaluObslugi = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.NORTH, kosztKanaluObslugi, -3, SpringLayout.NORTH, lblKosztKanaluObslugi);
		springLayout.putConstraint(SpringLayout.EAST, kosztKanaluObslugi, -44, SpringLayout.EAST, frame.getContentPane());
		kosztKanaluObslugi.setModel(new SpinnerNumberModel(new Double(1), new Double(0), null, new Double(1)));
		frame.getContentPane().add(kosztKanaluObslugi);
		
		JLabel lblKoszt = new JLabel("Koszt zwi�zany z zadaniami znajduj�cymi si� w systemie:");
		springLayout.putConstraint(SpringLayout.NORTH, lblKoszt, 80, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblKoszt, 6, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblKoszt);
		
		final JSpinner koszt = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.EAST, lblKoszt, -24, SpringLayout.WEST, koszt);
		springLayout.putConstraint(SpringLayout.NORTH, koszt, 6, SpringLayout.SOUTH, kosztKanaluObslugi);
		springLayout.putConstraint(SpringLayout.WEST, koszt, 0, SpringLayout.WEST, kosztKanaluObslugi);
		koszt.setModel(new SpinnerNumberModel(new Double(4), new Double(0), null, new Double(1)));
		frame.getContentPane().add(koszt);
		
		JButton btnDalej = new JButton("Dalej");
		springLayout.putConstraint(SpringLayout.SOUTH, btnDalej, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnDalej, -35, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(btnDalej);
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		    	SpinnerModel model = kosztKanaluObslugi.getModel();
		    	double kosztKanalu = (Double) model.getValue();
		    	model = koszt.getModel();
		    	double jobCost = (Double)model.getValue();
		    	Queue q = new QueueWithLosses(settings.getQueue().getM(), settings.getQueue().getLambda(), settings.getQueue().getMi());
		    	JobCostTargetFunction targetFunction = new JobCostTargetFunction(q);
		    	targetFunction.setCostOfServerDepreciation(kosztKanalu);
		    	targetFunction.setJobCost(jobCost);
		    	settings.setTargetFunction(targetFunction);
		    	
		    	if(settings.getBee()!=null) new BeesWindow(settings);
		    	else if(settings.getFirefly()!=null) new FireFlyWindow(settings);
		    	else if(settings.getHybrid()!=null) new HybridWindow(settings);
		        frame.setVisible(false);
		    }
		});
		frame.setVisible(true);
	}

}
