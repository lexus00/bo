package gui;

import hybridAlgorithm.HASettings;
import main.Result;
import queues.Queue;
import targetFunctions.TargetFunction;
import beeAlgorithm.BASettings;
import fireflyAlgorithm.FASettings;

public class Settings {
	private BASettings bee = null;
	private FASettings firefly=null;
	private HASettings hybrid=null;
	private boolean fifo=true;
	private Queue queue;
	private TargetFunction targetFunction;
	private long timeBee=0;
	private long timeFirefly=0;
	private long timeHybrid=0;
	private Result beeResult;
	private Result fireflyResult;
	private Result hybridResult;
	private int iterationBee;
	private int iterationFirefly;
	private int iterationHybrid;
	
	public BASettings getBee() {
		return bee;
	}
	public void setBee(BASettings bee) {
		this.bee = bee;
	}

	public boolean isFifo() {
		return fifo;
	}
	public void setFifo(boolean fifo) {
		this.fifo = fifo;
	}
	public FASettings getFirefly() {
		return firefly;
	}
	public void setFirefly(FASettings firefly) {
		this.firefly = firefly;
	}

	public TargetFunction getTargetFunction() {
		return targetFunction;
	}
	public void setTargetFunction(TargetFunction targetFunction) {
		this.targetFunction = targetFunction;
	}
	public Queue getQueue() {
		return queue;
	}
	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	
	public HASettings getHybrid() {
		return hybrid;
	}
	public void setHybrid(HASettings hybrid) {
		this.hybrid = hybrid;
	}
	public long getTimeBee() {
		return timeBee;
	}
	public void setTimeBee(long timeBee) {
		this.timeBee = timeBee;
	}
	public long getTimeFirefly() {
		return timeFirefly;
	}
	public void setTimeFirefly(long timeFirefly) {
		this.timeFirefly = timeFirefly;
	}
	public long getTimeHybrid() {
		return timeHybrid;
	}
	public void setTimeHybrid(long timeHybrid) {
		this.timeHybrid = timeHybrid;
	}
	public Result getBeeResult() {
		return beeResult;
	}
	public void setBeeResult(Result beeResult) {
		this.beeResult = beeResult;
	}
	public Result getFireflyResult() {
		return fireflyResult;
	}
	public void setFireflyResult(Result fireflyResult) {
		this.fireflyResult = fireflyResult;
	}
	public Result getHybridResult() {
		return hybridResult;
	}
	public void setHybridResult(Result hybridResult) {
		this.hybridResult = hybridResult;
	}
	public int getIterationBee() {
		return iterationBee;
	}
	public void setIterationBee(int iterationBee) {
		this.iterationBee = iterationBee;
	}
	public int getIterationFirefly() {
		return iterationFirefly;
	}
	public void setIterationFirefly(int iterationFirefly) {
		this.iterationFirefly = iterationFirefly;
	}
	public int getIterationHybrid() {
		return iterationHybrid;
	}
	public void setIterationHybrid(int iterationHybrid) {
		this.iterationHybrid = iterationHybrid;
	}


}
