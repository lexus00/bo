package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JLabel;

import queues.FixedSizeQueue;
import queues.Queue;
import queues.QueueWithLosses;

public class ResultsWindow {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public ResultsWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Settings settings) {
			frame = new JFrame();
			frame.setBounds(100, 100, 750, 275);
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[]{100, 4, 66, 82, 16, 128, 6, 0};
			gridBagLayout.rowHeights = new int[]{14, 14, 14, 14, 14, 14, 14, 14, 14, 0, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			frame.getContentPane().setLayout(gridBagLayout);
				
				JLabel lblA = new JLabel("ajsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva");
				lblA.setForeground(SystemColor.menu);
				GridBagConstraints gbc_lblAjsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva = new GridBagConstraints();
				gbc_lblAjsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva.insets = new Insets(0, 0, 5, 5);
				gbc_lblAjsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva.gridx = 0;
				gbc_lblAjsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva.gridy = 0;
				
				frame.getContentPane().add(lblA, gbc_lblAjsghdfjdgsfashgfjagshfvaghsvfagshvagjvjdfghvajgfvasjdgfdsafdhva);
				
				

				JLabel lblPrzedOptymalizacja = new JLabel("Przed\n optymalizacja:");
				GridBagConstraints gbc_lblPrzedOptymalizacja = new GridBagConstraints();
				gbc_lblPrzedOptymalizacja.anchor = GridBagConstraints.NORTHWEST;
				gbc_lblPrzedOptymalizacja.insets = new Insets(0, 0, 5, 5);
				gbc_lblPrzedOptymalizacja.gridwidth = 2;
				gbc_lblPrzedOptymalizacja.gridx = 1;
				gbc_lblPrzedOptymalizacja.gridy = 0;
				frame.getContentPane().add(lblPrzedOptymalizacja, gbc_lblPrzedOptymalizacja);
				
				JLabel lblAlgorytmPszczeli = new JLabel("Algorytm pszczeli");
				GridBagConstraints gbc_lblAlgorytmPszczeli = new GridBagConstraints();
				gbc_lblAlgorytmPszczeli.anchor = GridBagConstraints.NORTH;
				gbc_lblAlgorytmPszczeli.insets = new Insets(0, 0, 5, 5);
				gbc_lblAlgorytmPszczeli.gridx = 3;
				gbc_lblAlgorytmPszczeli.gridy = 0;
				
				
				JLabel lblAlgorytmSwietlika = new JLabel("Algorytm swietlika");
				GridBagConstraints gbc_lblAlgorytmSwietlika = new GridBagConstraints();
				gbc_lblAlgorytmSwietlika.anchor = GridBagConstraints.NORTH;
				gbc_lblAlgorytmSwietlika.insets = new Insets(0, 0, 5, 5);
				gbc_lblAlgorytmSwietlika.gridx = 4;
				gbc_lblAlgorytmSwietlika.gridy = 0;		
				
				JLabel lblHybryda = new JLabel("Hybryda");
				GridBagConstraints gbc_lblHybryda = new GridBagConstraints();
				gbc_lblHybryda.anchor = GridBagConstraints.WEST;
				gbc_lblHybryda.insets = new Insets(0, 0, 5, 5);
				gbc_lblHybryda.gridx = 5;
				gbc_lblHybryda.gridy = 0;
	
								
				//Etykiety z nazwami parametrow
				JLabel lblPrawdopodobienstwoOdmowy = new JLabel("prawdopodobienstwo odmowy:");
				GridBagConstraints gbc_lblPrawdopodobienstwoOdmowy = new GridBagConstraints();
				gbc_lblPrawdopodobienstwoOdmowy.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblPrawdopodobienstwoOdmowy.gridwidth = 2;
				gbc_lblPrawdopodobienstwoOdmowy.anchor = GridBagConstraints.NORTH;
				gbc_lblPrawdopodobienstwoOdmowy.insets = new Insets(0, 0, 5, 5);
				gbc_lblPrawdopodobienstwoOdmowy.gridx = 0;
				gbc_lblPrawdopodobienstwoOdmowy.gridy = 1;
				frame.getContentPane().add(lblPrawdopodobienstwoOdmowy, gbc_lblPrawdopodobienstwoOdmowy);
				
				JLabel lblSredniRozmiarSystemu = new JLabel("srednia liczba zgloszen w systemie:");
				GridBagConstraints gbc_lblSredniRozmiarSystemu = new GridBagConstraints();
				gbc_lblSredniRozmiarSystemu.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblSredniRozmiarSystemu.gridwidth = 2;
				gbc_lblSredniRozmiarSystemu.anchor = GridBagConstraints.NORTH;
				gbc_lblSredniRozmiarSystemu.insets = new Insets(0, 0, 5, 5);
				gbc_lblSredniRozmiarSystemu.gridx = 0;
				gbc_lblSredniRozmiarSystemu.gridy = 2;
				frame.getContentPane().add(lblSredniRozmiarSystemu, gbc_lblSredniRozmiarSystemu);
				
				JLabel lblSredniCzasOczekiwania = new JLabel("sredni czas oczekiwania w kolejce:");
				GridBagConstraints gbc_lblSredniCzasOczekiwania = new GridBagConstraints();
				gbc_lblSredniCzasOczekiwania.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblSredniCzasOczekiwania.gridwidth = 2;
				gbc_lblSredniCzasOczekiwania.anchor = GridBagConstraints.NORTH;
				gbc_lblSredniCzasOczekiwania.insets = new Insets(0, 0, 5, 5);
				gbc_lblSredniCzasOczekiwania.gridx = 0;
				gbc_lblSredniCzasOczekiwania.gridy = 4;
				frame.getContentPane().add(lblSredniCzasOczekiwania, gbc_lblSredniCzasOczekiwania);
				
				JLabel lblSredniRozmiarKolejki = new JLabel("srednia liczba zgloszen w kolejce:");
				GridBagConstraints gbc_lblSredniRozmiarKolejki = new GridBagConstraints();
				gbc_lblSredniRozmiarKolejki.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblSredniRozmiarKolejki.gridwidth = 2;
				gbc_lblSredniRozmiarKolejki.anchor = GridBagConstraints.NORTH;
				gbc_lblSredniRozmiarKolejki.insets = new Insets(0, 0, 5, 5);
				gbc_lblSredniRozmiarKolejki.gridx = 0;
				gbc_lblSredniRozmiarKolejki.gridy = 3;
				frame.getContentPane().add(lblSredniRozmiarKolejki, gbc_lblSredniRozmiarKolejki);
				
				
				JLabel lblSredniCzasPrzebywania = new JLabel("sredni czas przebywania w systemie");
				GridBagConstraints gbc_lblSredniCzasPrzebywania = new GridBagConstraints();
				gbc_lblSredniCzasPrzebywania.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblSredniCzasPrzebywania.anchor = GridBagConstraints.NORTH;
				gbc_lblSredniCzasPrzebywania.insets = new Insets(0, 0, 5, 5);
				gbc_lblSredniCzasPrzebywania.gridwidth = 2;
				gbc_lblSredniCzasPrzebywania.gridx = 0;
				gbc_lblSredniCzasPrzebywania.gridy = 5;
				frame.getContentPane().add(lblSredniCzasPrzebywania, gbc_lblSredniCzasPrzebywania);
				
				JLabel lblFunkcjaCelu = new JLabel("funkcja celu:");
				GridBagConstraints gbc_lblFunkcjaCelu = new GridBagConstraints();
				gbc_lblFunkcjaCelu.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblFunkcjaCelu.gridwidth = 2;
				gbc_lblFunkcjaCelu.anchor = GridBagConstraints.NORTH;
				gbc_lblFunkcjaCelu.insets = new Insets(0, 0, 5, 5);
				gbc_lblFunkcjaCelu.gridx = 0;
				gbc_lblFunkcjaCelu.gridy = 6;
				frame.getContentPane().add(lblFunkcjaCelu, gbc_lblFunkcjaCelu);
				
				JLabel lblRo = new JLabel("ro:");
				GridBagConstraints gbc_lblRo = new GridBagConstraints();
				gbc_lblRo.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblRo.gridwidth = 2;
				gbc_lblRo.anchor = GridBagConstraints.NORTH;
				gbc_lblRo.insets = new Insets(0, 0, 5, 5);
				gbc_lblRo.gridx = 0;
				gbc_lblRo.gridy = 7;
				frame.getContentPane().add(lblRo, gbc_lblRo);
		
				JLabel lblM = new JLabel("m:");
				GridBagConstraints gbc_lblM = new GridBagConstraints();
				gbc_lblM.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblM.gridwidth = 2;
				gbc_lblM.anchor = GridBagConstraints.NORTH;
				gbc_lblM.insets = new Insets(0, 0, 5, 5);
				gbc_lblM.gridx = 0;
				gbc_lblM.gridy = 8;
				frame.getContentPane().add(lblM, gbc_lblM);
				
				JLabel lblLambda = new JLabel ("lamda:");
				GridBagConstraints gbc_lblLambda = new GridBagConstraints();
				gbc_lblLambda.anchor = GridBagConstraints.NORTH;
				gbc_lblLambda.fill=GridBagConstraints.HORIZONTAL;
				gbc_lblLambda.gridwidth=2;
				gbc_lblLambda.gridx=0;
				gbc_lblLambda.gridy=9;
				frame.getContentPane().add(lblLambda, gbc_lblLambda);
				
				JLabel lblCzasDzialaniaAlgorytmu = new JLabel("czas dzialania algorytmu[ms]:");
				GridBagConstraints gbc_lblCzasDzialaniaAlgorytmu = new GridBagConstraints();
				gbc_lblCzasDzialaniaAlgorytmu.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblCzasDzialaniaAlgorytmu.insets = new Insets(0, 0, 0, 5);
				gbc_lblCzasDzialaniaAlgorytmu.gridx = 0;
				gbc_lblCzasDzialaniaAlgorytmu.gridy = 10;
				frame.getContentPane().add(lblCzasDzialaniaAlgorytmu, gbc_lblCzasDzialaniaAlgorytmu);	
				
				JLabel lblTheBestIteration = new JLabel("Najlepszy wynik znaleziony w iteracji:");
				GridBagConstraints gbc_lblTheBestIteration = new GridBagConstraints();
				gbc_lblTheBestIteration.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblTheBestIteration.gridx=0;
				gbc_lblTheBestIteration.gridy=11;
				frame.getContentPane().add(lblTheBestIteration, gbc_lblTheBestIteration);
				
				//Parametry przed optymalizacja
				
				JLabel rejectionProbability = new JLabel("0");
				rejectionProbability.setText(format(settings.getQueue().probabilityOfRejection())+"");
				GridBagConstraints gbc_rejectionProbability = new GridBagConstraints();
				gbc_rejectionProbability.anchor = GridBagConstraints.WEST;
				gbc_rejectionProbability.insets = new Insets(0, 0, 5, 5);
				gbc_rejectionProbability.gridx = 2;
				gbc_rejectionProbability.gridy = 1;
				frame.getContentPane().add(rejectionProbability, gbc_rejectionProbability);
				
				JLabel systemSize = new JLabel("0");
				systemSize.setText(format(settings.getQueue().averageSystemSize())+"");
				
				GridBagConstraints gbc_systemSize = new GridBagConstraints();
				gbc_systemSize.anchor = GridBagConstraints.NORTHWEST;
				gbc_systemSize.insets = new Insets(0, 0, 5, 5);
				gbc_systemSize.gridx = 2;
				gbc_systemSize.gridy = 2;
				frame.getContentPane().add(systemSize, gbc_systemSize);
							
				
				JLabel queueTime = new JLabel();
				if(settings.isFifo()) {
				queueTime.setText(format(settings.getQueue().averageQueueWaitingTime())+"");
				System.out.println(settings.getQueue().averageQueueSize());}
				else queueTime.setText("-");
				GridBagConstraints gbc_queueTime = new GridBagConstraints();
				gbc_queueTime.anchor = GridBagConstraints.NORTHWEST;
				gbc_queueTime.insets = new Insets(0, 0, 5, 5);
				gbc_queueTime.gridx = 2;
				gbc_queueTime.gridy = 4;
				frame.getContentPane().add(queueTime, gbc_queueTime);			
			
				JLabel queueSize = new JLabel();
				if(settings.isFifo()) {queueSize.setText(format(settings.getQueue().averageQueueSize())+"");
				System.out.println(format(settings.getQueue().averageQueueSize()));
				}
				else queueSize.setText("-");
				GridBagConstraints gbc_queueSize = new GridBagConstraints();
				gbc_queueSize.anchor = GridBagConstraints.NORTHWEST;
				gbc_queueSize.insets = new Insets(0, 0, 5, 5);
				gbc_queueSize.gridx = 2;
				gbc_queueSize.gridy = 3;
				frame.getContentPane().add(queueSize, gbc_queueSize);	
			
				JLabel systemTime = new JLabel("0");
				systemTime.setText(format(settings.getQueue().averageTimeInSystem())+"");
				GridBagConstraints gbc_systemTime = new GridBagConstraints();
				gbc_systemTime.anchor = GridBagConstraints.NORTHWEST;
				gbc_systemTime.insets = new Insets(0, 0, 5, 5);
				gbc_systemTime.gridx = 2;
				gbc_systemTime.gridy = 5;
				frame.getContentPane().add(systemTime, gbc_systemTime);
				
				JLabel targetFunction = new JLabel("0");
				targetFunction.setText(format(settings.getTargetFunction().calculate(settings.getQueue().getM()).fValue)+"");
				GridBagConstraints gbc_targetFunction = new GridBagConstraints();
				gbc_targetFunction.gridwidth = 4;
				gbc_targetFunction.anchor = GridBagConstraints.NORTHWEST;
				gbc_targetFunction.insets = new Insets(0, 0, 5, 5);
				gbc_targetFunction.gridx = 2;
				gbc_targetFunction.gridy = 6;
				frame.getContentPane().add(targetFunction, gbc_targetFunction);
				
				JLabel ro = new JLabel("0");
				ro.setText(format(settings.getQueue().getRo())+"");
				GridBagConstraints gbc_ro = new GridBagConstraints();
				gbc_ro.anchor = GridBagConstraints.NORTHWEST;
				gbc_ro.insets = new Insets(0, 0, 5, 5);
				gbc_ro.gridx = 2;
				gbc_ro.gridy = 7;
				frame.getContentPane().add(ro, gbc_ro);
		
				JLabel m = new JLabel("0");
				m.setText(settings.getQueue().getM()+"");
				GridBagConstraints gbc_m = new GridBagConstraints();
				gbc_m.anchor = GridBagConstraints.NORTHWEST;
				gbc_m.insets = new Insets(0, 0, 5, 5);
				gbc_m.gridx = 2;
				gbc_m.gridy = 8;
				frame.getContentPane().add(m, gbc_m);
				
				JLabel lambda = new JLabel("0");
				lambda.setText(settings.getQueue().getLambda()+"");
				GridBagConstraints gbc_lambda = new GridBagConstraints();
				gbc_lambda.anchor = GridBagConstraints.WEST;
				gbc_lambda.gridx=2;
				gbc_lambda.gridy=9;
				frame.getContentPane().add(lambda, gbc_lambda);
				
//Parametry po optymalizacji - pszczolki	
				
				JLabel rejectionProbabilityBee = new JLabel("0");
				GridBagConstraints gbc_rejectionProbabilityBee = new GridBagConstraints();
				gbc_rejectionProbabilityBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_rejectionProbabilityBee.insets = new Insets(0, 0, 5, 5);
				gbc_rejectionProbabilityBee.gridx = 3;
				gbc_rejectionProbabilityBee.gridy = 1;
				
				JLabel systemSizeBee = new JLabel("0");
				GridBagConstraints gbc_systemSizeBee = new GridBagConstraints();
				gbc_systemSizeBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_systemSizeBee.insets = new Insets(0, 0, 5, 5);
				gbc_systemSizeBee.gridx = 3;
				gbc_systemSizeBee.gridy = 2;		
				
				JLabel queueSizeBee = new JLabel("0");
				GridBagConstraints gbc_queueSizeBee = new GridBagConstraints();
				gbc_queueSizeBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_queueSizeBee.insets = new Insets(0, 0, 5, 5);
				gbc_queueSizeBee.gridx = 3;
				gbc_queueSizeBee.gridy = 3;
				
				JLabel queueTimeBee = new JLabel("0");				
				GridBagConstraints gbc_queueTimeBee = new GridBagConstraints();
				gbc_queueTimeBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_queueTimeBee.insets = new Insets(0, 0, 5, 5);
				gbc_queueTimeBee.gridx = 3;
				gbc_queueTimeBee.gridy = 4;
				
				JLabel systemTimeBee = new JLabel("0");		
				GridBagConstraints gbc_systemTimeBee = new GridBagConstraints();
				gbc_systemTimeBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_systemTimeBee.insets = new Insets(0, 0, 5, 5);
				gbc_systemTimeBee.gridx = 3;
				gbc_systemTimeBee.gridy = 5;
						
				JLabel mBee = new JLabel("0");				
				GridBagConstraints gbc_mBee = new GridBagConstraints();
				gbc_mBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_mBee.insets = new Insets(0, 0, 0, 5);
				gbc_mBee.gridx = 3;
				gbc_mBee.gridy = 8;
				
				JLabel lamdaBee = new JLabel("0");
				GridBagConstraints gbc_lambdaBee = new GridBagConstraints();
				gbc_lambdaBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_lambdaBee.gridx=3;
				gbc_lambdaBee.gridy=9;
				
				JLabel roBee = new JLabel("0");
				GridBagConstraints gbc_roBee = new GridBagConstraints();
				gbc_roBee.anchor = GridBagConstraints.NORTHWEST;
				gbc_roBee.gridx=3;
				gbc_roBee.gridy=7;
				
				
				JLabel lblCzasDzialaniaBee = new JLabel("czasDzialaniaBee");
				GridBagConstraints gbc_lblCzasdzialaniabee = new GridBagConstraints();
				gbc_lblCzasdzialaniabee.anchor = GridBagConstraints.WEST;
				gbc_lblCzasdzialaniabee.insets = new Insets(0, 0, 0, 5);
				gbc_lblCzasdzialaniabee.gridx = 3;
				gbc_lblCzasdzialaniabee.gridy = 10;
				
				JLabel lblTargetFunctionBee = new JLabel("targetFunctionBee");
				GridBagConstraints gbc_lblTargetFunctionBee = new GridBagConstraints();
				gbc_lblTargetFunctionBee.anchor = GridBagConstraints.WEST;
				gbc_lblTargetFunctionBee.insets = new Insets(0, 0, 0, 5);
				gbc_lblTargetFunctionBee.gridx = 3;
				gbc_lblTargetFunctionBee.gridy = 6;
				
				JLabel lblTheBestIterationBee = new JLabel("Najlepszy wynik znaleziony w iteracji:");
				GridBagConstraints gbc_lblTheBestIterationBee = new GridBagConstraints();
				gbc_lblTheBestIterationBee.anchor = GridBagConstraints.WEST;
				gbc_lblTheBestIterationBee.gridy = 11;
				gbc_lblTheBestIterationBee.gridx = 3;
				gbc_lblTheBestIteration.gridx=3;
				gbc_lblTheBestIteration.gridy=11;
				
		if(settings.getBee()!=null) {	
			Queue q;	
			if(settings.isFifo()) {				   		
		   		q=new FixedSizeQueue(settings.getBeeResult().m,settings.getQueue().getN(), settings.getQueue().getLambda(), settings.getQueue().getMi());
			}else  q=new QueueWithLosses(settings.getBeeResult().m,settings.getQueue().getLambda(), settings.getQueue().getMi());
			
			lblTheBestIterationBee.setText(settings.getIterationBee()+"");
			frame.getContentPane().add(lblTheBestIterationBee, gbc_lblTheBestIterationBee);
			
			lamdaBee.setText(format(q.getLambda())+"");
			frame.getContentPane().add(lamdaBee, gbc_lambdaBee);
			
			roBee.setText(format(q.getRo())+"");
			frame.getContentPane().add(roBee, gbc_roBee);
			
			lblTargetFunctionBee.setText(format(settings.getBeeResult().fValue)+"");
			frame.getContentPane().add(lblTargetFunctionBee, gbc_lblTargetFunctionBee);
			
			mBee.setText(settings.getBeeResult().m+"");	
			frame.getContentPane().add(mBee, gbc_mBee);
			
			systemTimeBee.setText(format(q.averageTimeInSystem())+"");
			frame.getContentPane().add(systemTimeBee, gbc_systemTimeBee);
			
			
			if(settings.isFifo()) {						
		   		queueTimeBee.setText(format(q.averageQueueWaitingTime())+"");
			}
			else queueTimeBee.setText("-");
			frame.getContentPane().add(queueTimeBee, gbc_queueTimeBee);
			
			systemSizeBee.setText(format(q.averageSystemSize())+"");			
			frame.getContentPane().add(systemSizeBee, gbc_systemSizeBee);
			
			if(settings.isFifo())queueSizeBee.setText(format(q.averageQueueSize())+"");
			else queueSizeBee.setText("-");
			frame.getContentPane().add(queueSizeBee, gbc_queueSizeBee);
		
			frame.getContentPane().add(lblAlgorytmPszczeli, gbc_lblAlgorytmPszczeli);
					
			rejectionProbabilityBee.setText(format(q.probabilityOfRejection())+"");				
			frame.getContentPane().add(rejectionProbabilityBee, gbc_rejectionProbabilityBee);	
			
			lblCzasDzialaniaBee.setText(settings.getTimeBee()+"");
			frame.getContentPane().add(lblCzasDzialaniaBee, gbc_lblCzasdzialaniabee);
			
		}
		
//Parametry po optymalizacji - swietliki	
		JLabel probabilityOfRejection = new JLabel("0");
		GridBagConstraints gbc_probabilityOfRejection = new GridBagConstraints();
		gbc_probabilityOfRejection.anchor = GridBagConstraints.NORTHWEST;
		gbc_probabilityOfRejection.insets = new Insets(0, 0, 5, 5);
		gbc_probabilityOfRejection.gridx = 4;
		gbc_probabilityOfRejection.gridy = 1;
						
		JLabel systemSizeFirefly = new JLabel("0");			
		GridBagConstraints gbc_systemSizeFirefly = new GridBagConstraints();
		gbc_systemSizeFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_systemSizeFirefly.insets = new Insets(0, 0, 5, 5);
		gbc_systemSizeFirefly.gridx = 4;
		gbc_systemSizeFirefly.gridy = 2;
		
		JLabel queueSizeFirefly = new JLabel("0");		
		GridBagConstraints gbc_queueSizeFirefly = new GridBagConstraints();
		gbc_queueSizeFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_queueSizeFirefly.insets = new Insets(0, 0, 5, 5);
		gbc_queueSizeFirefly.gridx = 4;
		gbc_queueSizeFirefly.gridy = 3;		
		
		JLabel queueTimeFirefly = new JLabel("0");				
		GridBagConstraints gbc_queueTimeFirefly = new GridBagConstraints();
		gbc_queueTimeFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_queueTimeFirefly.insets = new Insets(0, 0, 5, 5);
		gbc_queueTimeFirefly.gridx = 4;
		gbc_queueTimeFirefly.gridy = 4;		
		
		JLabel systemTimeFirefly = new JLabel("0");		
		GridBagConstraints gbc_systemTimeFirefly = new GridBagConstraints();
		gbc_systemTimeFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_systemTimeFirefly.insets = new Insets(0, 0, 5, 5);
		gbc_systemTimeFirefly.gridx = 4;
		gbc_systemTimeFirefly.gridy = 5;
		
		JLabel mFirefly = new JLabel("0");		
		GridBagConstraints gbc_mFirefly = new GridBagConstraints();
		gbc_mFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_mFirefly.insets = new Insets(0, 0, 0, 5);
		gbc_mFirefly.gridx = 4;
		gbc_mFirefly.gridy = 8;	
		
		JLabel lambdaFirefly = new JLabel("0");
		GridBagConstraints gbc_lambdaFirefly = new GridBagConstraints();
		gbc_lambdaFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_lambdaFirefly.gridx=4;
		gbc_lambdaFirefly.gridy=9;
		
		JLabel roFirefly = new JLabel("0");
		GridBagConstraints gbc_roFirefly = new GridBagConstraints();
		gbc_roFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_roFirefly.gridx=4;
		gbc_roFirefly.gridy=7;
		
		JLabel lblCzasDzialaniaFirefly = new JLabel("czasDzialaniaFirefly");
		GridBagConstraints gbc_lblCzasdzialaniafirefly = new GridBagConstraints();
		gbc_lblCzasdzialaniafirefly.anchor = GridBagConstraints.WEST;
		gbc_lblCzasdzialaniafirefly.insets = new Insets(0, 0, 0, 5);
		gbc_lblCzasdzialaniafirefly.gridx = 4;
		gbc_lblCzasdzialaniafirefly.gridy = 10;
		
		JLabel lblTargetFunctionFirefly = new JLabel("targetFunctionFirefly");
		GridBagConstraints gbc_lblTargetFunctionFirefly = new GridBagConstraints();
		gbc_lblTargetFunctionFirefly.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblTargetFunctionFirefly.insets = new Insets(0, 0, 0, 5);
		gbc_lblTargetFunctionFirefly.gridx = 4;
		gbc_lblTargetFunctionFirefly.gridy = 6;
		
			
		JLabel lblTheBestIterationFirefly = new JLabel();
		GridBagConstraints gbc_lblTheBestIterationFirefly = new GridBagConstraints();
		gbc_lblTheBestIterationFirefly.anchor = GridBagConstraints.WEST;
		gbc_lblTheBestIterationFirefly.gridx=4;
		gbc_lblTheBestIterationFirefly.gridy=11;
		
		if (settings.getFirefly()!=null){
			Queue qFirefly;
			if(settings.isFifo()) qFirefly=new FixedSizeQueue(settings.getFireflyResult().m,settings.getQueue().getN(), settings.getQueue().getLambda(), settings.getQueue().getMi());
	   		else  qFirefly=new QueueWithLosses(settings.getFireflyResult().m,settings.getQueue().getLambda(), settings.getQueue().getMi());
			
			lblTheBestIterationFirefly.setText(settings.getIterationFirefly()+"");
			frame.getContentPane().add(lblTheBestIterationFirefly, gbc_lblTheBestIterationFirefly);
			
			lambdaFirefly.setText(format(qFirefly.getLambda())+"");
			frame.getContentPane().add(lambdaFirefly, gbc_lambdaFirefly);
			
			roFirefly.setText(format(qFirefly.getRo())+"");
			frame.getContentPane().add(roFirefly, gbc_roFirefly);
			
			lblTargetFunctionFirefly.setText(format(settings.getFireflyResult().fValue)+"");			
			frame.getContentPane().add(lblTargetFunctionFirefly, gbc_lblTargetFunctionFirefly);
			
			lblCzasDzialaniaFirefly.setText(settings.getTimeFirefly()+"");
			frame.getContentPane().add(lblCzasDzialaniaFirefly, gbc_lblCzasdzialaniafirefly);
			
			mFirefly.setText(settings.getFireflyResult().m+"");
			frame.getContentPane().add(mFirefly, gbc_mFirefly);
			
			systemTimeFirefly.setText(format(qFirefly.averageTimeInSystem())+"");
			frame.getContentPane().add(systemTimeFirefly, gbc_systemTimeFirefly);
			
			if(settings.isFifo())queueTimeFirefly.setText(format(qFirefly.averageQueueWaitingTime())+"");
			else queueTimeFirefly.setText("-");
			frame.getContentPane().add(queueTimeFirefly, gbc_queueTimeFirefly);
			
			if(settings.isFifo())queueSizeFirefly.setText(format(qFirefly.averageQueueSize())+"");
			else queueSizeFirefly.setText("-");
			frame.getContentPane().add(queueSizeFirefly, gbc_queueSizeFirefly);
			
			frame.getContentPane().add(lblAlgorytmSwietlika, gbc_lblAlgorytmSwietlika);
			
			systemSizeFirefly.setText(format(qFirefly.averageSystemSize())+"");
			frame.getContentPane().add(systemSizeFirefly, gbc_systemSizeFirefly);		
			
			probabilityOfRejection.setText(format(qFirefly.probabilityOfRejection())+"");							
			frame.getContentPane().add(probabilityOfRejection, gbc_probabilityOfRejection);	
		}
		
		
//Parametry po optymalizacji - hybryda
				JLabel rejectionProbabilityHybrid = new JLabel("0");
				GridBagConstraints gbc_label_16 = new GridBagConstraints();
				gbc_label_16.anchor = GridBagConstraints.NORTHWEST;
				gbc_label_16.insets = new Insets(0, 0, 5, 5);
				gbc_label_16.gridx = 5;
				gbc_label_16.gridy = 1;
				

				JLabel systemSizeHybrid = new JLabel("0");
				GridBagConstraints gbc_label_17 = new GridBagConstraints();
				gbc_label_17.anchor = GridBagConstraints.NORTHWEST;
				gbc_label_17.insets = new Insets(0, 0, 5, 5);
				gbc_label_17.gridx = 5;
				gbc_label_17.gridy = 2;
				
				JLabel queueSizeHybrid = new JLabel("0");
				GridBagConstraints gbc_label_18 = new GridBagConstraints();
				gbc_label_18.anchor = GridBagConstraints.NORTHWEST;
				gbc_label_18.insets = new Insets(0, 0, 5, 5);
				gbc_label_18.gridx = 5;
				gbc_label_18.gridy = 3;
				
				JLabel queueTimeHybrid = new JLabel("0");
				GridBagConstraints gbc_label_19 = new GridBagConstraints();
				gbc_label_19.anchor = GridBagConstraints.NORTHWEST;
				gbc_label_19.insets = new Insets(0, 0, 5, 5);
				gbc_label_19.gridx = 5;
				gbc_label_19.gridy = 4;
				
				JLabel systemTimeHybrid = new JLabel("0");
				GridBagConstraints gbc_label_20 = new GridBagConstraints();
				gbc_label_20.anchor = GridBagConstraints.NORTHWEST;
				gbc_label_20.insets = new Insets(0, 0, 5, 5);
				gbc_label_20.gridx = 5;
				gbc_label_20.gridy = 5;
				
				JLabel mHybrid = new JLabel("0");
				GridBagConstraints gbc_mHybrid = new GridBagConstraints();
				gbc_mHybrid.insets = new Insets(0, 0, 5, 5);
				gbc_mHybrid.anchor = GridBagConstraints.NORTHWEST;
				gbc_mHybrid.gridx = 5;
				gbc_mHybrid.gridy = 8;
			
				JLabel lambdaHybrid = new JLabel("0");
				GridBagConstraints gbc_lambdaHybrid = new GridBagConstraints();
				gbc_lambdaHybrid.anchor = GridBagConstraints.NORTHWEST;
				gbc_lambdaHybrid.gridx=5;
				gbc_lambdaHybrid.gridy=9;
				
				JLabel roHybrid = new JLabel("0");
				GridBagConstraints gbc_roHybrid = new GridBagConstraints();
				gbc_roHybrid.anchor = GridBagConstraints.NORTHWEST;
				gbc_roHybrid.gridx=5;
				gbc_roHybrid.gridy=7;
				
				
				JLabel czasDzialaniaHybrid = new JLabel("0");
				GridBagConstraints gbc_lblCzasdzialaniahybrid = new GridBagConstraints();
				gbc_lblCzasdzialaniahybrid.anchor = GridBagConstraints.WEST;
				gbc_lblCzasdzialaniahybrid.insets = new Insets(0, 0, 0, 5);
				gbc_lblCzasdzialaniahybrid.gridx = 5;
				gbc_lblCzasdzialaniahybrid.gridy = 10;
				
				JLabel targetFunctionHybrid = new JLabel("0");
				GridBagConstraints gbc_targetFunctionHybrid = new GridBagConstraints();
				gbc_targetFunctionHybrid.anchor = GridBagConstraints.WEST;
				gbc_targetFunctionHybrid.insets = new Insets(0, 0, 0, 5);
				gbc_targetFunctionHybrid.gridx = 5;
				gbc_targetFunctionHybrid.gridy = 6;
				
				
				JLabel lblTheBestIterationHybrid = new JLabel();
				GridBagConstraints gbc_lblTheBestIterationHybrid = new GridBagConstraints();
				gbc_lblTheBestIterationHybrid.anchor = GridBagConstraints.WEST;
				gbc_lblTheBestIterationHybrid.gridx=5;
				gbc_lblTheBestIterationHybrid.gridy=11;
				
			if(settings.getHybrid()!=null) {
				Queue qHybrid;
				if(settings.isFifo()) qHybrid=new FixedSizeQueue(settings.getHybridResult().m,settings.getQueue().getN(), settings.getQueue().getLambda(), settings.getQueue().getMi());
			   	else  qHybrid=new QueueWithLosses(settings.getHybridResult().m,settings.getQueue().getLambda(), settings.getQueue().getMi());
				
				lblTheBestIterationHybrid.setText(settings.getIterationHybrid()+"");
				frame.getContentPane().add(lblTheBestIterationHybrid, gbc_lblTheBestIterationHybrid);
				
				lambdaHybrid.setText(format(qHybrid.getLambda())+"");
				frame.getContentPane().add(lambdaHybrid, gbc_lambdaHybrid);
				
				roHybrid.setText(format(qHybrid.getRo())+"");
				frame.getContentPane().add(roHybrid, gbc_roHybrid);
				
				targetFunctionHybrid.setText(format(settings.getHybridResult().fValue)+"");
				frame.getContentPane().add(targetFunctionHybrid, gbc_targetFunctionHybrid);
				
				frame.getContentPane().add(lblHybryda, gbc_lblHybryda);
				czasDzialaniaHybrid.setText(settings.getTimeHybrid()+"");
				frame.getContentPane().add(czasDzialaniaHybrid, gbc_lblCzasdzialaniahybrid);		
				
				rejectionProbabilityHybrid.setText(format(qHybrid.probabilityOfRejection())+"");
				frame.getContentPane().add(rejectionProbabilityHybrid, gbc_label_16);	
		
				systemSizeHybrid.setText(format(qHybrid.averageSystemSize())+"");
				frame.getContentPane().add(systemSizeHybrid, gbc_label_17);		
		
				if(settings.isFifo())queueSizeHybrid.setText(format(qHybrid.averageQueueSize())+"");
				else queueSizeHybrid.setText("-");
				frame.getContentPane().add(queueSizeHybrid, gbc_label_18);	
		
				if(settings.isFifo())queueTimeHybrid.setText(format(qHybrid.averageQueueWaitingTime())+"");
				else queueTimeHybrid.setText("-");
				frame.getContentPane().add(queueTimeHybrid, gbc_label_19);	
		
				systemTimeHybrid.setText(format(qHybrid.averageTimeInSystem())+"");
				frame.getContentPane().add(systemTimeHybrid, gbc_label_20);	
			
				mHybrid.setText(qHybrid.getM()+"");
				frame.getContentPane().add(mHybrid, gbc_mHybrid);
			}
				
				
				
		
		frame.setVisible(true);
	}
	private static double format(double liczba) {
		liczba*=1000000;
		liczba=Math.round(liczba);
		liczba/=1000000;
		
		return liczba;
	}
}
