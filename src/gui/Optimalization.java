package gui;

import hybridAlgorithm.HybridAlgorithm;
import beeAlgorithm.BeesAlgorithm;
import fireflyAlgorithm.FireflyAlgorithm;

public class Optimalization {
	public void startOptimalization(Settings settings){
		if(settings.getBee()!=null) {
			BeesAlgorithm bee = new BeesAlgorithm(settings.getBee(), settings.getTargetFunction());
			long timeBee=System.currentTimeMillis();
	   		bee.run();
	   		settings.setTimeBee(System.currentTimeMillis()-timeBee);
	   	
	   		settings.setBeeResult(bee.getMaxProfit());
	   		settings.setIterationBee(bee.getIterationWhenFindBestResult());
		}
		
		if(settings.getFirefly()!=null) {
			
			FireflyAlgorithm firefly = new FireflyAlgorithm(settings.getFirefly(), settings.getTargetFunction());
			long timeFirefly=System.currentTimeMillis();
			firefly.run();
			settings.setTimeFirefly(System.currentTimeMillis()-timeFirefly);
			settings.setFireflyResult(firefly.getMaxProfit());
			settings.setIterationFirefly(firefly.getIterationWhenFindBestResult());
			
		}
		
		if(settings.getHybrid()!=null) {
			HybridAlgorithm hybrid = new HybridAlgorithm(settings.getHybrid(), settings.getTargetFunction());
			long timeHybrid=System.currentTimeMillis();
			hybrid.run();	
			settings.setTimeHybrid(System.currentTimeMillis()-timeHybrid);
			settings.setHybridResult(hybrid.getMaxProfit());
			settings.setIterationHybrid(hybrid.getIterationWhenFindBestResult());
		}
		new ResultsWindow(settings);
	}
}
