package gui;

import hybridAlgorithm.HASettings;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import beeAlgorithm.BASettings;
import fireflyAlgorithm.FASettings;

public class AlgorithmWindow {

	private JFrame frame;
	private Settings settings;

	/**
	 * Create the application.
	 */
	public AlgorithmWindow(Settings settings) {
		this.setSettings(settings);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 202);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblKtrychAlgorytmowChcesz = new JLabel("Kt\u00F3rych algorytmow chcesz uzyc do optymalizacji?");
		springLayout.putConstraint(SpringLayout.NORTH, lblKtrychAlgorytmowChcesz, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblKtrychAlgorytmowChcesz, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblKtrychAlgorytmowChcesz);
		
		final JCheckBox beeCheckBox = new JCheckBox("Algorytm pszczeli");
		springLayout.putConstraint(SpringLayout.NORTH, beeCheckBox, 15, SpringLayout.SOUTH, lblKtrychAlgorytmowChcesz);
		springLayout.putConstraint(SpringLayout.WEST, beeCheckBox, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(beeCheckBox);
		
		final JCheckBox fireflyCheckBox = new JCheckBox("Algorytm swietlika");
		springLayout.putConstraint(SpringLayout.NORTH, fireflyCheckBox, 6, SpringLayout.SOUTH, beeCheckBox);
		springLayout.putConstraint(SpringLayout.WEST, fireflyCheckBox, 0, SpringLayout.WEST, lblKtrychAlgorytmowChcesz);
		frame.getContentPane().add(fireflyCheckBox);
		
		final JCheckBox hybridCheckBox = new JCheckBox("Hybryda swietlikow i algorytmu pszczelego");
		springLayout.putConstraint(SpringLayout.NORTH, hybridCheckBox, 6, SpringLayout.SOUTH, fireflyCheckBox);
		springLayout.putConstraint(SpringLayout.WEST, hybridCheckBox, 0, SpringLayout.WEST, lblKtrychAlgorytmowChcesz);
		frame.getContentPane().add(hybridCheckBox);
		
		
		final JLabel lbl = new JLabel("Musisz wybrac przynajmniej jeden algorytm!");
		lbl.setVisible(false);
		lbl.setForeground(Color.RED);
		springLayout.putConstraint(SpringLayout.NORTH, lbl, 6, SpringLayout.SOUTH, hybridCheckBox);
		springLayout.putConstraint(SpringLayout.WEST, lbl, 10, SpringLayout.WEST, hybridCheckBox);
		frame.getContentPane().add(lbl);
		
		JButton btnDalej = new JButton("Dalej");
		springLayout.putConstraint(SpringLayout.NORTH, btnDalej, -33, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnDalej, -100, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnDalej, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnDalej, -37, SpringLayout.EAST, frame.getContentPane());
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		    	if(!beeCheckBox.isSelected()&& !fireflyCheckBox.isSelected()&&!hybridCheckBox.isSelected()) {
		    		lbl.setVisible(true);
		    	}else {
		    		
		    		if (beeCheckBox.isSelected())settings.setBee(new BASettings());
			    	if (fireflyCheckBox.isSelected()) settings.setFirefly(new FASettings());
			    	if (hybridCheckBox.isSelected()) settings.setHybrid(new HASettings());
			    	
			    	new QueueWindow(settings);
			    	frame.setVisible(false);
		    	}
		    }
		});
		frame.getContentPane().add(btnDalej);
	
		
	
		frame.setVisible(true);

		
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}
}
