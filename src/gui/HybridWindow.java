package gui;

import hybridAlgorithm.HASettings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class HybridWindow {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public HybridWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(final Settings settings) {
		frame = new JFrame();
		frame.setBounds(100, 100, 824, 508);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblPodajDaneDla = new JLabel("Podaj dane dla algorytmu hybrydy:");
		frame.getContentPane().add(lblPodajDaneDla, "2, 2");
		
		JLabel lblLiczbaPszczolN = new JLabel("liczba pszczol n");
		frame.getContentPane().add(lblLiczbaPszczolN, "2, 4");
		
		final JSpinner liczbaPszczolN = new JSpinner(new SpinnerNumberModel());
		liczbaPszczolN.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaPszczolN, "6, 4");
		
		ImageIcon icon = new ImageIcon("graphics/hybrid.gif");
		JLabel lblImage = new JLabel(icon);
		lblImage.setHorizontalAlignment(SwingConstants.RIGHT);
	
		frame.getContentPane().add(lblImage, "10, 5, 1, 21, center, center");
		
		JLabel lblLiczbaMiejscM = new JLabel("liczba miejsc m wybranych sposrod n odwiedzonych");
		frame.getContentPane().add(lblLiczbaMiejscM, "2, 6");
		
		JSpinner liczbaMiejscM = new JSpinner(new SpinnerNumberModel());
		liczbaMiejscM.setModel(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaMiejscM, "6, 6");
		
		JLabel lblLiczbaNajlepszychMiejsc = new JLabel("liczba najlepszych miejsc e spo\u015Br\u00F3d m");
		frame.getContentPane().add(lblLiczbaNajlepszychMiejsc, "2, 8");
		
		final JSpinner liczbaNajlepszychMiejscE = new JSpinner(new SpinnerNumberModel());
		liczbaNajlepszychMiejscE.setModel(new SpinnerNumberModel(new Integer(3), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaNajlepszychMiejscE, "6, 8");
		
		JLabel lblLiczbaPszczRekrutowanych = new JLabel("liczba pszcz\u00F3\u0142 rekrutowanych do e najlepszych miejsc\t");
		frame.getContentPane().add(lblLiczbaPszczRekrutowanych, "2, 10");
		
		final JSpinner liczbaPszczolDoE = new JSpinner(new SpinnerNumberModel());
		liczbaPszczolDoE.setModel(new SpinnerNumberModel(new Integer(40), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaPszczolDoE, "6, 10");
		
		JLabel lblLiczbaPszczRekrutowana = new JLabel("liczba pszcz\u00F3\u0142 rekrutowana do m-e gorszych miejsc");
		frame.getContentPane().add(lblLiczbaPszczRekrutowana, "2, 12");
		
		final JSpinner liczbaPszczolDoME = new JSpinner(new SpinnerNumberModel());
		liczbaPszczolDoME.setModel(new SpinnerNumberModel(new Integer(20), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaPszczolDoME, "6, 12");
		
		JLabel lblRozmiarSsiedztwaW = new JLabel("rozmiar s\u0105siedztwa w %");
		frame.getContentPane().add(lblRozmiarSsiedztwaW, "2, 14");
		
		final JSpinner rozmiarSasiedztwaS = new JSpinner(new SpinnerNumberModel());
		rozmiarSasiedztwaS.setModel(new SpinnerNumberModel(0.0, 0.0, 100.0, 0.0));
		rozmiarSasiedztwaS.setValue(1.1);
		frame.getContentPane().add(rozmiarSasiedztwaS, "6, 14");
		
		JLabel lblLiczbaIteracji = new JLabel("liczba iteracji");
		frame.getContentPane().add(lblLiczbaIteracji, "2, 16");
		
		final JSpinner liczbaIteracji = new JSpinner(new SpinnerNumberModel());
		liczbaIteracji.setModel(new SpinnerNumberModel(new Integer(500), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaIteracji, "6, 16");
		
		JLabel lblCoIleIteracji = new JLabel("co ile iteracji ma by\u0107 zapisany najlepszy obecny rezultat?");
		frame.getContentPane().add(lblCoIleIteracji, "2, 18");
		
		final JSpinner krokZapisu = new JSpinner(new SpinnerNumberModel());
		krokZapisu.setModel(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(krokZapisu, "6, 18");
		
		JLabel lblDolneOgraniczenieIlosci = new JLabel("dolne ograniczenie ilosci kanalow");
		frame.getContentPane().add(lblDolneOgraniczenieIlosci, "2, 20");
		
		final JSpinner dolneOgrM = new JSpinner(new SpinnerNumberModel());
		frame.getContentPane().add(dolneOgrM, "6, 20");
		
		JLabel lblGorneOgraniczenieIlosci = new JLabel("gorne ograniczenie ilosci kanalow");
		frame.getContentPane().add(lblGorneOgraniczenieIlosci, "2, 22");
		
		final JSpinner gorneOgrM = new JSpinner(new SpinnerNumberModel());
		gorneOgrM.setModel(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(gorneOgrM, "6, 22");
		
		
		JLabel lblAbsorbcjaSwiatlaGamma = new JLabel("Absorbcja swiatla gamma");
		frame.getContentPane().add(lblAbsorbcjaSwiatlaGamma, "2, 24");
		
		final JSpinner absorbcjaSwiatlaGamma = new JSpinner(new SpinnerNumberModel(new Double(1), null, null, new Double(1)));
		frame.getContentPane().add(absorbcjaSwiatlaGamma, "6, 24");
		
		JLabel lblMaksymalnaAtrakcyjnosc = new JLabel("Maksymalna atrakcyjnosc:");
		frame.getContentPane().add(lblMaksymalnaAtrakcyjnosc, "2, 26");
		
		final JSpinner maxAtrakcyjnosc = new JSpinner(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		maxAtrakcyjnosc.setValue(0.5);
		frame.getContentPane().add(maxAtrakcyjnosc, "6, 26");
		
		JLabel lblAlfa = new JLabel("Alfa");
		frame.getContentPane().add(lblAlfa, "2, 28");
		
		final JSpinner alfa = new JSpinner(new SpinnerNumberModel());
		alfa.setValue(1.1);
		frame.getContentPane().add(alfa, "6,28");
		
		JLabel lblLiczbaIteracjiSwietlik = new JLabel("Liczba iteracji:");
		frame.getContentPane().add(lblLiczbaIteracjiSwietlik, "2, 30");
		
		final JSpinner liczbaIteracjiSwietlik = new JSpinner(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaIteracjiSwietlik, "6, 30");
		
		JLabel lblRozmiarOtoczeniaDla = new JLabel("Rozmiar otoczenia dla swietlikow");
		frame.getContentPane().add(lblRozmiarOtoczeniaDla, "2, 32");
		
		final JSpinner otoczenieSwietliki = new JSpinner();
		otoczenieSwietliki.setModel(new SpinnerNumberModel(new Integer(2), null, null, new Integer(1)));
		frame.getContentPane().add(otoczenieSwietliki, "6, 32");
		
		
		JLabel label = new JLabel("");
		frame.getContentPane().add(label, "2, 24");
		
		JButton btnDalej = new JButton("Dalej");
		
		
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		       	HASettings hybrid = new HASettings();
		      
		       	SpinnerNumberModel model ;
				hybrid.lambda=settings.getQueue().getLambda();
				model = (SpinnerNumberModel) liczbaNajlepszychMiejscE.getModel();
				hybrid.e=(Integer) model.getValue();
				hybrid.m=settings.getQueue().getM();
				hybrid.mi=settings.getQueue().getMi();
			 	model = (SpinnerNumberModel) gorneOgrM.getModel();
				hybrid.mMax=(Integer) model.getValue();
				model=(SpinnerNumberModel) dolneOgrM.getModel();
				hybrid.mMin=(Integer) model.getValue();
				model = (SpinnerNumberModel) liczbaPszczolN.getModel();
				hybrid.n=(Integer) model.getValue();
				model = (SpinnerNumberModel) liczbaPszczolDoE.getModel();
				hybrid.nep = (Integer) model.getValue();
				model = (SpinnerNumberModel) rozmiarSasiedztwaS.getModel();
				hybrid.ngh=(Double)model.getValue();
				model = (SpinnerNumberModel) liczbaPszczolDoME.getModel();
				hybrid.nsp=(Integer) model.getValue();
				model = (SpinnerNumberModel) liczbaIteracji.getModel();
				hybrid.numberOfIterations=(Integer) model.getValue();
				model = (SpinnerNumberModel) krokZapisu.getModel();
				hybrid.saveEvery=(Integer)model.getValue();
				
				model=(SpinnerNumberModel) absorbcjaSwiatlaGamma.getModel();
				hybrid.lightAbsorbtion=(Double)model.getValue();
				
				model =(SpinnerNumberModel) maxAtrakcyjnosc.getModel();
				hybrid.maxAttractiveness=(Double)model.getValue();
				
				model = (SpinnerNumberModel) alfa.getModel();
				hybrid.alfa=(Double)model.getValue();
				
				model = (SpinnerNumberModel) liczbaIteracjiSwietlik.getModel();
				hybrid.numberOfIterationsForFireflies=(Integer)model.getValue();
						
				model = (SpinnerNumberModel)otoczenieSwietliki.getModel();
				hybrid.neighbourhoodRadius=(Integer)model.getValue();
				
				settings.setHybrid(hybrid);
				
		       	Optimalization o = new Optimalization();
			  	o.startOptimalization(settings);		      
		       
		       frame.setVisible(false);
		    }
		});
		
		frame.getContentPane().add(btnDalej, "8, 34");
		frame.setVisible(true);
	}

}
