package gui;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import queues.FixedSizeQueue;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;
import targetFunctions.JobProfitTargetFunction;

public class QueueWindow {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public QueueWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(final Settings settings) {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 202);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblPodajDane = new JLabel("Podaj dane:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPodajDane, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblPodajDane, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblPodajDane);
		
		JLabel lblM = new JLabel("m");
		springLayout.putConstraint(SpringLayout.WEST, lblM, 0, SpringLayout.WEST, lblPodajDane);
		frame.getContentPane().add(lblM);
		
		JLabel lblMi = new JLabel("mi");
		springLayout.putConstraint(SpringLayout.WEST, lblMi, 0, SpringLayout.WEST, lblPodajDane);
		frame.getContentPane().add(lblMi);
		
		JLabel lblLambda = new JLabel("lambda");
		springLayout.putConstraint(SpringLayout.WEST, lblLambda, 0, SpringLayout.WEST, lblPodajDane);
		frame.getContentPane().add(lblLambda);
		
		JLabel lblN = new JLabel("N");
		springLayout.putConstraint(SpringLayout.WEST, lblN, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblN, -15, SpringLayout.SOUTH, frame.getContentPane());
		
		final JSpinner mSpinner = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.WEST, mSpinner, 104, SpringLayout.EAST, lblM);
		springLayout.putConstraint(SpringLayout.SOUTH, mSpinner, -110, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, mSpinner, -267, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblM, 3, SpringLayout.NORTH, mSpinner);
		mSpinner.setModel(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(mSpinner);				
		
		final JSpinner miSpinner = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.WEST, miSpinner, 102, SpringLayout.EAST, lblMi);
		springLayout.putConstraint(SpringLayout.EAST, miSpinner, -267, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblMi, 3, SpringLayout.NORTH, miSpinner);
		springLayout.putConstraint(SpringLayout.NORTH, miSpinner, 11, SpringLayout.SOUTH, mSpinner);
		miSpinner.setModel(new SpinnerNumberModel(new Double(2), null, null, new Double(1)));
		frame.getContentPane().add(miSpinner);
		
		final JSpinner lambdaSpinner = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.NORTH, lblLambda, 3, SpringLayout.NORTH, lambdaSpinner);
		springLayout.putConstraint(SpringLayout.NORTH, lambdaSpinner, 13, SpringLayout.SOUTH, miSpinner);
		springLayout.putConstraint(SpringLayout.WEST, lambdaSpinner, 0, SpringLayout.WEST, mSpinner);
		springLayout.putConstraint(SpringLayout.EAST, lambdaSpinner, 0, SpringLayout.EAST, mSpinner);
		lambdaSpinner.setModel(new SpinnerNumberModel(new Double(3), null, null, new Double(1)));
		frame.getContentPane().add(lambdaSpinner);
		
		final JSpinner nSpinner = new JSpinner(new SpinnerNumberModel());
		springLayout.putConstraint(SpringLayout.EAST, lblN, -105, SpringLayout.WEST, nSpinner);
		springLayout.putConstraint(SpringLayout.WEST, nSpinner, 0, SpringLayout.WEST, mSpinner);
		springLayout.putConstraint(SpringLayout.EAST, nSpinner, 0, SpringLayout.EAST, mSpinner);
		nSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		lblN.setVisible(false);
		nSpinner.setValue(1);;
		nSpinner.setVisible(false);
		frame.getContentPane().add(lblN);	
		frame.getContentPane().add(nSpinner);
		if(settings.isFifo()) {							
			nSpinner.setVisible(true);
			lblN.setVisible(true);
		}
			
		JButton btnDalej = new JButton("Dalej");
		springLayout.putConstraint(SpringLayout.NORTH, nSpinner, 1, SpringLayout.NORTH, btnDalej);
		springLayout.putConstraint(SpringLayout.SOUTH, btnDalej, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnDalej, -42, SpringLayout.EAST, frame.getContentPane());
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		    	SpinnerModel model = lambdaSpinner.getModel();
		    	double lambda = (Double) model.getValue();
		    	model = mSpinner.getModel();
		    	int m =(Integer) model.getValue();
		    	model = miSpinner.getModel();
		    	double mi =(Double) model.getValue();
		    	model = nSpinner.getModel();
		    	int n=(Integer)model.getValue();
		    	if(settings.isFifo()) {
		    		settings.setQueue(new FixedSizeQueue(m, n, lambda, mi));
		    		settings.setTargetFunction(new JobProfitTargetFunction(settings.getQueue()));
		    		new FifoTargetFunctionWindow(settings);
		    	}else {
		    		settings.setQueue(new QueueWithLosses(m, lambda, mi));
		    		settings.setTargetFunction(new JobCostTargetFunction(settings.getQueue()));
		    		new TargetFunctionWindow(settings);
		    	}
		    	
		        frame.setVisible(false);
		    }
		});
		frame.getContentPane().add(btnDalej);
		frame.setVisible(true);
	}	
}
