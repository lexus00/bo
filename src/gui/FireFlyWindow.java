package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fireflyAlgorithm.FASettings;

public class FireFlyWindow {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public FireFlyWindow(Settings settings) {
		initialize(settings);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(final Settings settings) {
		frame = new JFrame();
		frame.setBounds(100, 100, 699, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblPodajDaneDla = new JLabel("Podaj dane dla algorytmu \u015Bwietlika:");
		frame.getContentPane().add(lblPodajDaneDla, "2, 2");
		
		JLabel lblLiczbaSwietlikowN = new JLabel("Liczba swietlikow n");
		frame.getContentPane().add(lblLiczbaSwietlikowN, "2, 6");
		
		final JSpinner liczbaSwietlikowN = new JSpinner(new SpinnerNumberModel(new Integer(30), null, null, new Integer(1)));
		frame.getContentPane().add(liczbaSwietlikowN, "6, 6");
		
		JLabel lblAbsorbcjaSwiatlaGamma = new JLabel("Absorbcja swiatla gamma");
		frame.getContentPane().add(lblAbsorbcjaSwiatlaGamma, "2, 8");
		
		final JSpinner absorbcjaSwiatlaGamma = new JSpinner(new SpinnerNumberModel(new Double(1), null, null, new Double(1)));
		frame.getContentPane().add(absorbcjaSwiatlaGamma, "6, 8");
		
		JLabel lblMaksymalnaAtrakcyjnosc = new JLabel("Maksymalna atrakcyjnosc:");
		frame.getContentPane().add(lblMaksymalnaAtrakcyjnosc, "2, 10");
		
		final JSpinner maxAtrakcyjnosc = new JSpinner(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		maxAtrakcyjnosc.setValue(0.5);
		frame.getContentPane().add(maxAtrakcyjnosc, "6, 10");
		
		JLabel lblAlfa = new JLabel("Alfa");
		frame.getContentPane().add(lblAlfa, "2, 12");
		
		final JSpinner alfa = new JSpinner(new SpinnerNumberModel());
		alfa.setValue(1.1);
		frame.getContentPane().add(alfa, "6, 12");
		
		ImageIcon icon = new ImageIcon("graphics/firefly.gif");
		JLabel lblNewLabel = new JLabel(icon);
		frame.getContentPane().add(lblNewLabel, "10, 7, 1, 17, center, center");
		
		JLabel lblLiczbaIteracji = new JLabel("Liczba iteracji:");
		frame.getContentPane().add(lblLiczbaIteracji, "2, 14");
		
		final JSpinner liczbaIteracji = new JSpinner(new SpinnerNumberModel(new Integer(10), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(liczbaIteracji, "6, 14");
		
		JLabel lblDolneOgraniczenieIlosc = new JLabel("Dolne ograniczenie ilosc kanalow:");
		frame.getContentPane().add(lblDolneOgraniczenieIlosc, "2, 16");
		
		final JSpinner dolneOgrM = new JSpinner(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
		frame.getContentPane().add(dolneOgrM, "6, 16");
		
		JLabel lblGorneOgraniczenieIlosc = new JLabel("Gorne ograniczenie ilosc kanalow:");
		frame.getContentPane().add(lblGorneOgraniczenieIlosc, "2, 18");
		
		final JSpinner gorneOgrM = new JSpinner(new SpinnerNumberModel(50, 0, 50, 1));
		frame.getContentPane().add(gorneOgrM, "6, 18");
		
		JButton btnDalej = new JButton("Dalej");
		frame.getContentPane().add(btnDalej, "8, 22");
		btnDalej.addActionListener(new ActionListener() {
		    //This method will be called whenever you click the button.
		    public void actionPerformed(ActionEvent e) {
		       	FASettings firefly = new FASettings();
		       	SpinnerNumberModel model ;
				model=(SpinnerNumberModel) liczbaSwietlikowN.getModel();
				firefly.n=(Integer)model.getValue();
				
				model=(SpinnerNumberModel) absorbcjaSwiatlaGamma.getModel();
				firefly.lightAbsorbtion=(Double)model.getValue();
				
				model =(SpinnerNumberModel) maxAtrakcyjnosc.getModel();
				firefly.maxAttractiveness=(Double)model.getValue();
				
				model = (SpinnerNumberModel) alfa.getModel();
				firefly.alfa=(Double)model.getValue();
				
				model = (SpinnerNumberModel) liczbaIteracji.getModel();
				firefly.maxIterations=(Integer)model.getValue();
				
				model = (SpinnerNumberModel) dolneOgrM.getModel();
				firefly.mMin=(Integer)model.getValue();
				
				model = (SpinnerNumberModel) gorneOgrM.getModel();
				firefly.mMax=(Integer)model.getValue();
				settings.setFirefly(firefly);
		       if(settings.getHybrid()!=null) new HybridWindow(settings);
		       else {
		    	Optimalization o = new Optimalization();
		    	o.startOptimalization(settings);
		       }
		       frame.setVisible(false);
		       
		    }
		});
	

		frame.setVisible(true);
	}

		
	
}
