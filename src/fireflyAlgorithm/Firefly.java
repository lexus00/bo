package fireflyAlgorithm;


public class Firefly {
	private FASettings settings;
	private int position;

	Firefly(FASettings settings) {
		this.settings = settings;
	}
	
	public double getAttractiveness(Firefly firefly) {
		double distance = Math.abs(this.position - firefly.getPosition());
		return (settings.maxAttractiveness * Math.exp(- settings.lightAbsorbtion * Math.pow(distance, 2)))
				+ (1 - settings.maxAttractiveness);
	}
	
	public double getBasicLightIntensity() {
		return settings.fun.calculate(position).fValue;
	}
	
	public void moveTowards(Firefly firefly) {
		int distance = firefly.getPosition() - this.position;
		int newPosition = (int) Math.round(this.position + getAttractiveness(firefly) * distance
				+ settings.alfa * (settings.rand.nextDouble() - 0.5));
		this.setPosition(newPosition);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int pos) {
		if ( pos >= settings.mMin && pos <= settings.mMax )
			this.position = pos;
		else if ( pos < settings.mMin )
			this.position = settings.mMin;
		else
			this.position = settings.mMax;
	}

}