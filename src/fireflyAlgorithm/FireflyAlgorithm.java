package fireflyAlgorithm;

import main.IAlgorithm;
import targetFunctions.TargetFunction;
import main.Result;

import java.util.ArrayList;

public class FireflyAlgorithm extends Thread implements IAlgorithm{
	private Firefly fireflies[];
	private FASettings settings;
	private boolean finished;
	private boolean cancel;
	private double light_intensities[];
	private double max_intensity;
	private double globalMaxIntensity;
	private Result maxProfit;
	private ArrayList<Result> profits;
	private int bestResultInIteration;


	public FireflyAlgorithm(FASettings settings, TargetFunction fun) {
		this.settings = settings;
		settings.fun = fun;
		cancel = false;
		finished = false;
		profits = new ArrayList<Result>();
		maxProfit = new Result();
		light_intensities = new double[settings.n];
		fireflies = new Firefly[settings.n];
		for ( int i = 0; i < settings.n; i++ )
			fireflies[i] = new Firefly(settings);
		max_intensity = 0;
		globalMaxIntensity = 0;
	}

	public void run() {

		double step = (settings.mMax-settings.mMin)/(double)(settings.n-1);
		for(int i=0; i<settings.n; i++){		//inicjalizacja �wietlik�w
			//fireflies[i].setPosition(settings.rand.nextInt(settings.mMax) + 1);   // random
			fireflies[i].setPosition(settings.mMin + (int)(i*step));
		}

//		for(int i=0; i<settings.n; i++){		//to test
//			System.out.print(fireflies[i].getPosition()+ " ");
//			System.out.print(fireflies[i].getBasicLightIntensity()+ " ");
//		}
//		System.out.println();

		int best_firefly=0;
		double attractiveness_j;
		double attractiveness_k;
		
		for ( int i = 0; i < settings.maxIterations; i++ ) {

			for ( int j = 0; j < settings.n; j++ ) {
				for ( int k = 0; k < settings.n; k++ ) {
					attractiveness_j = fireflies[j].getBasicLightIntensity();
					attractiveness_k = fireflies[k].getBasicLightIntensity();
					if (attractiveness_j < attractiveness_k) {
						fireflies[j].moveTowards(fireflies[k]);
					}
				}
			}

			max_intensity = 0;
			best_firefly = 0;
			for(int j=0; j < settings.n; j++){
				light_intensities[j]=fireflies[j].getBasicLightIntensity();
				if(light_intensities[j]>max_intensity){
					max_intensity=light_intensities[j];
					best_firefly=j;
				}
			}
			if ( max_intensity > globalMaxIntensity ) {
				globalMaxIntensity = max_intensity;
				bestResultInIteration = i + 1;
			}
//			System.out.println("Iteration:" + i);
//			System.out.println("Best firefly:" + best_firefly + " " + fireflies[best_firefly].getPosition() + " " + fireflies[best_firefly].getBasicLightIntensity());
			profits.add(new Result(fireflies[best_firefly].getPosition(), max_intensity, -1, -1));

			if ( cancel )
				break;

//			for(int ii=0; ii<settings.n; ii++){		//to test
//				System.out.print(fireflies[ii].getPosition() + " ");
//			}
//			System.out.println();

			
		}
		maxProfit = new Result(fireflies[best_firefly].getPosition(), max_intensity, -1, -1);
		finished = true;
	}

	public boolean isFinished(){
		return finished;
	}

	public void cancel(){
		cancel = true;
	}

	@Override
	public Result getMaxProfit() {
		return maxProfit;
	}

	@Override
	public ArrayList<Result> getProfits() {
		return profits;
	}
	
	public int getIterationWhenFindBestResult() {
		return bestResultInIteration;
	}
	
	public ArrayList<Integer> getFirefliesPosition(){
		ArrayList<Integer> result = new ArrayList<Integer>();	
		for(Firefly firefly : fireflies){
			result.add(firefly.getPosition());
		}
		return result;
	}

}