package fireflyAlgorithm;

import targetFunctions.TMPFunction;
import targetFunctions.TargetFunction;

import java.util.Random;

public class FASettings {
	public int n;  // liczba �wietlik�w
	public double lightAbsorbtion;  // Gamma
	public double maxAttractiveness;  // Beta0
	public double alfa; 
	
	public TargetFunction fun;
	public int maxIterations;
	public int mMin; // dolne ograniczenie ilosci kanalow
	public int mMax; // gorne ograniczenie ilosci kanalow
	Random rand;
	
	public FASettings() {
		n = 20;
		maxIterations = 40;
		maxAttractiveness = 0.9;
		alfa = 0.1;
		lightAbsorbtion = 1;
		mMin = 1;
		mMax = 50;
		rand = new Random();
		fun = new TMPFunction();
	}
	
	public FASettings(int n, int maxIterations, double maxAttractiveness, double alfa, double lightAbsorbtion, int mMin, int mMax) {
		this.n = n;
		this.maxIterations = maxIterations;
		this.maxAttractiveness = maxAttractiveness;
		this.alfa = alfa;
		this.lightAbsorbtion = lightAbsorbtion;
		this.mMin = mMin;
		this.mMax = mMax;
		rand = new Random();
		fun = new TMPFunction();
	}
}