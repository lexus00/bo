package main;

/**
 * Created by maciek on 05.01.2014.
 */
public class TestCase {
    private double mi;
    private double lambda;
    private int m;
    private double costFunction;
    private int numberOfIterations;
    private int averageSystemSize;
    long timeOfExecution;

    public double getMi() {
        return mi;
    }

    public double getLambda() {
        return lambda;
    }

    public int getM() {
        return m;
    }

    public double getCostFunction() {
        return costFunction;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public int getAverageSystemSize() {
        return averageSystemSize;
    }

    public long getTimeOfExecution() {
        return timeOfExecution;
    }

    public TestCase(double mi, double lambda, int m, double costFunction, int numberOfIterations, int averageSystemSize, long timeOfExecution) {

        this.mi = mi;
        this.lambda = lambda;
        this.m = m;
        this.costFunction = costFunction;
        this.numberOfIterations = numberOfIterations;
        this.averageSystemSize = averageSystemSize;
        this.timeOfExecution = timeOfExecution;
    }
}
