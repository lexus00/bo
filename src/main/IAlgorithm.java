package main;

import java.util.ArrayList;

public interface IAlgorithm {
	public Result getMaxProfit();
	public ArrayList<Result> getProfits();
	public boolean isFinished();
	public void run();
	public void cancel();
	public int getIterationWhenFindBestResult();
}
