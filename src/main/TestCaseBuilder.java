package main;

public class TestCaseBuilder {
    private double mi;
    private double lambda;
    private int m;
    private double costFunction;
    private int numberOfIterations;
    private int averageSystemSize;
    private long timeOfExecution;

    public TestCaseBuilder setMi(double mi) {
        this.mi = mi;
        return this;
    }

    public TestCaseBuilder setLambda(double lambda) {
        this.lambda = lambda;
        return this;
    }

    public TestCaseBuilder setM(int m) {
        this.m = m;
        return this;
    }

    public TestCaseBuilder setCostFunction(double costFunction) {
        this.costFunction = costFunction;
        return this;
    }

    public TestCaseBuilder setNumberOfIterations(int numberOfIterations) {
        this.numberOfIterations = numberOfIterations;
        return this;
    }

    public TestCaseBuilder setAverageSystemSize(int averageSystemSize) {
        this.averageSystemSize = averageSystemSize;
        return this;
    }

    public TestCaseBuilder setTimeOfExecution(long timeOfExecution) {
        this.timeOfExecution = timeOfExecution;
        return this;
    }

    public TestCase createTestCase() {
        return new TestCase(mi, lambda, m, costFunction, numberOfIterations, averageSystemSize, timeOfExecution);
    }
}