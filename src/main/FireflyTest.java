package main;

import fireflyAlgorithm.FASettings;
import fireflyAlgorithm.FireflyAlgorithm;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;
import targetFunctions.TargetFunction;


public class FireflyTest {

	public static void main(String[] args) {
		FASettings settings = new FASettings();
		TargetFunction fun = new JobCostTargetFunction(new QueueWithLosses(1, 10, 15));
		FireflyAlgorithm alg = new FireflyAlgorithm(settings, fun);
		alg.run();
		System.out.println(alg.getMaxProfit().fValue+" m = "+ alg.getMaxProfit().m +" nr iter = " + alg.getIterationWhenFindBestResult());
		}
	
}
