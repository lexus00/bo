package main;

import beeAlgorithm.BASettings;
import beeAlgorithm.BeesAlgorithm;
import fireflyAlgorithm.FASettings;
import fireflyAlgorithm.FireflyAlgorithm;
import hybridAlgorithm.HASettings;
import hybridAlgorithm.HybridAlgorithm;
import queues.FixedSizeQueue;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;
import targetFunctions.JobProfitTargetFunction;
import targetFunctions.TargetFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maciek on 05.01.2014.
 */
public class Statistics {
    private final BASettings baSet;
    private final FASettings faSet;
    private final HASettings haSet;
    private List<TestCase> testCases = new ArrayList<TestCase>();

    public Statistics(BASettings baSet, FASettings faSet, HASettings haSet) {
        createTestCases();
        this.baSet = baSet;
        this.faSet = faSet;
        this.haSet = haSet;

        runTest();
    }

    private void runTest() {
        System.out.println("JobCostTargetFunction");
        System.out.println("lambda | mi | m | bee profit | bee iter | firefly profit | firefly iter | hybrid profit | hybrid iter | initial cost");
        for(TestCase testCase : testCases) {
            TargetFunction func = new JobCostTargetFunction(new QueueWithLosses(testCase.getM(), testCase.getLambda(), testCase.getMi()));
            IAlgorithm alg = new BeesAlgorithm(baSet , func);
            IAlgorithm alg2 = new FireflyAlgorithm(faSet, func);
            IAlgorithm alg3 = new HybridAlgorithm(haSet, func);
            alg.run();
            alg2.run();
            alg3.run();
//            System.out.println("bee " + alg.getMaxProfit().fValue+" m = "+ alg.getMaxProfit().m +" nr iter = " + alg.getIterationWhenFindBestResult());
//            System.out.println("firefly " +alg2.getMaxProfit().fValue+" m = "+ alg2.getMaxProfit().m + " nr iter = "+alg2.getIterationWhenFindBestResult());
//            System.out.println("hybrid " +alg3.getMaxProfit().fValue+" m = "+ alg3.getMaxProfit().m + " nr iter = "+alg3.getIterationWhenFindBestResult());
            System.out.println(String.format("%f | %f | %d | %f | %d | %f | %d | %f | %d | %f",
                    testCase.getLambda(), testCase.getMi(), testCase.getM(),
                    alg.getMaxProfit().fValue, alg.getIterationWhenFindBestResult(),
                    alg2.getMaxProfit().fValue, alg2.getIterationWhenFindBestResult(),
                    alg3.getMaxProfit().fValue, alg3.getIterationWhenFindBestResult() ,
                    func.calculate(testCase.getM()).fValue
            ));
        }

        System.out.println("JobProfitTargetFunction");
        System.out.println("lambda | mi | m | bee profit | bee iter | firefly profit | firefly iter | hybrid profit | hybrid iter | initial cost");
        for(TestCase testCase : testCases) {
            // let's assume that N is equal to 3 * m
            TargetFunction func = new JobProfitTargetFunction(new FixedSizeQueue(testCase.getM(), testCase.getM() * 3, testCase.getLambda(), testCase.getMi()));
            IAlgorithm alg = new BeesAlgorithm(baSet , func);
            IAlgorithm alg2 = new FireflyAlgorithm(faSet, func);
            IAlgorithm alg3 = new HybridAlgorithm(haSet, func);
            alg.run();
            alg2.run();
            alg3.run();
//            System.out.println("bee " + alg.getMaxProfit().fValue+" m = "+ alg.getMaxProfit().m +" nr iter = " + alg.getIterationWhenFindBestResult());
//            System.out.println("firefly " +alg2.getMaxProfit().fValue+" m = "+ alg2.getMaxProfit().m + " nr iter = "+alg2.getIterationWhenFindBestResult());
//            System.out.println("hybrid " +alg3.getMaxProfit().fValue+" m = "+ alg3.getMaxProfit().m + " nr iter = "+alg3.getIterationWhenFindBestResult());
            System.out.println(String.format("%f | %f | %d | %f | %d | %f | %d | %f | %d | %f",
                    testCase.getLambda(), testCase.getMi(), testCase.getM(),
                    alg.getMaxProfit().fValue, alg.getIterationWhenFindBestResult(),
                    alg2.getMaxProfit().fValue, alg2.getIterationWhenFindBestResult(),
                    alg3.getMaxProfit().fValue, alg3.getIterationWhenFindBestResult(),
                    func.calculate(testCase.getM()).fValue
            ));
        }

    }

    private void createTestCases() {
        testCases.add(new TestCaseBuilder().setLambda(1).setMi(1).setM(2).createTestCase());
        testCases.add(new TestCaseBuilder().setLambda(10).setMi(5).setM(3).createTestCase());
        testCases.add(new TestCaseBuilder().setLambda(10).setMi(10).setM(2).createTestCase());
        testCases.add(new TestCaseBuilder().setLambda(10).setMi(15).setM(1).createTestCase());
        testCases.add(new TestCaseBuilder().setLambda(25).setMi(5).setM(8).createTestCase());
        testCases.add(new TestCaseBuilder().setLambda(50).setMi(5).setM(14).createTestCase());
    }
}