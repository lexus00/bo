package main;

import beeAlgorithm.BASettings;
import beeAlgorithm.BeesAlgorithm;
import fireflyAlgorithm.FASettings;
import fireflyAlgorithm.FireflyAlgorithm;
import hybridAlgorithm.HASettings;
import hybridAlgorithm.HybridAlgorithm;
import queues.FixedSizeQueue;
import queues.QueueWithLosses;
import targetFunctions.JobCostTargetFunction;
import targetFunctions.JobProfitTargetFunction;
import targetFunctions.TMPFunction;
import targetFunctions.TargetFunction;


public class Main {

	public static void main(String[] args) {
//      some tests form M/M/m/-/m
//		System.out.println("QueueWithLosses(8, 25, 5)");
        TargetFunction f1 = new JobCostTargetFunction(new QueueWithLosses(8, 25, 5));
        /*       System.out.println("f1(8) = " +f1.calculate(8).fValue);
        System.out.println("f1(30) = " +f1.calculate(30).fValue);
        System.out.println("f1(45) = " +f1.calculate(45).fValue);
*/
        TargetFunction f2 = new JobProfitTargetFunction(new QueueWithLosses(8, 25, 5));
/*        System.out.println("f2(8) = " + f2.calculate(8).fValue);
        System.out.println("f2(30) = " +f2.calculate(30).fValue);
        System.out.println("f1(45) = " +f2.calculate(45).fValue);
*/		BASettings beeSettings = new BASettings();
		FASettings fireflySettings = new FASettings();
		HASettings hybridSettings = new HASettings();
		
		hybridSettings.mMin = beeSettings.mMin = fireflySettings.mMin;
		hybridSettings.mMax = beeSettings.mMax = fireflySettings.mMax;
		hybridSettings.searchMax = beeSettings.searchMax = true;
		
		System.out.println("f1");
		test(beeSettings,fireflySettings,hybridSettings,f1);
		System.out.println("f2");
		test(beeSettings,fireflySettings,hybridSettings,f2);
		
		System.out.println("--------------------------------------");
//      some tests form M/M/m/FiFo/m+N
	//	System.out.println("\nFixedSizeQueue(10,40,25,5)");
        f1 = new JobCostTargetFunction(new FixedSizeQueue(10,10,25,5));
/*        System.out.println("f1(8) = " +f1.calculate(8).fValue);
        System.out.println("f1(30) = " +f1.calculate(30).fValue);
        System.out.println("f1(45) = " +f1.calculate(45).fValue);
*/
        f2 = new JobProfitTargetFunction(new FixedSizeQueue(10,40,25,5));
 /*      System.out.println("f2(8) = " + f2.calculate(8).fValue);
        System.out.println("f2(30) = " +f2.calculate(30).fValue);
        System.out.println("f2(45) = " +f2.calculate(45).fValue);
*/
		beeSettings.searchMax = true;
		System.out.println("f1");
		test(beeSettings,fireflySettings,hybridSettings,f1);
		System.out.println("f2");
		test(beeSettings,fireflySettings,hybridSettings,f2); 
		System.out.println("--------------------------------------");
		beeSettings.searchMax = true;
		System.out.println("\nfunkcja liniowa f(x) = x szukane maximum przedzia� "+ beeSettings.mMin +" - "+beeSettings.mMax);
		test(beeSettings,fireflySettings,hybridSettings,new TMPFunction());
	}
	
	private static void test(BASettings baSet, FASettings faSet, TargetFunction func){
		IAlgorithm alg = new BeesAlgorithm(baSet , func);
		IAlgorithm alg2 = new FireflyAlgorithm(faSet, func);
		alg.run();
		alg2.run();
		System.out.println("bee " + alg.getMaxProfit().fValue+" m = "+ alg.getMaxProfit().m +" nr iter = " + alg.getIterationWhenFindBestResult());
		System.out.println("firefly " +alg2.getMaxProfit().fValue+" m = "+ alg2.getMaxProfit().m + " nr iter = "+alg2.getIterationWhenFindBestResult());
	}
	private static void test(BASettings baSet, FASettings faSet, HASettings haSet, TargetFunction func){
		IAlgorithm alg = new BeesAlgorithm(baSet , func);
		IAlgorithm alg2 = new FireflyAlgorithm(faSet, func);
		IAlgorithm alg3 = new HybridAlgorithm(haSet, func);
		alg.run();
		alg2.run();
		alg3.run();
		System.out.println("bee " + alg.getMaxProfit().fValue+" m = "+ alg.getMaxProfit().m +" nr iter = " + alg.getIterationWhenFindBestResult());
		System.out.println("firefly " +alg2.getMaxProfit().fValue+" m = "+ alg2.getMaxProfit().m + " nr iter = "+alg2.getIterationWhenFindBestResult());
		System.out.println("hybrid " +alg3.getMaxProfit().fValue+" m = "+ alg3.getMaxProfit().m + " nr iter = "+alg3.getIterationWhenFindBestResult());
	//    new Statistics(baSet, faSet, haSet);

    }
}
