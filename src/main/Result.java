package main;

import java.util.Comparator;

public class Result implements Comparator<Result>{
	public final int m; // liczba kanal�w
	public final double fValue; // wynik f(m)
	public final double Podm;
	public final double K; // �rednia ilo�� zg�osze� w kolejce
	
							// warto tu by by�o doda� wszystki parametry kolejki kt�re
							// b�d� przy okazji wyliczone i b�dzie trzeba je pokaza�
	
	public Result(int m, double fValue, double Podm, double K){
		this.fValue = fValue;
		this.Podm = Podm;
		this.K = K;
		this.m = m;
	}
	
	public Result(){  // poczatkowa wartosc dla pszcz�
		this.m = -1;
		this.fValue = -1;
		this.Podm = -1;
		this.K = -1;
	}

	@Override
	public int compare(Result o1, Result o2) {
		if( o1.fValue < o2.fValue)
			return 1;
		if( o1.fValue == o2.fValue)
			return 0;
		return -1;
	}

	
}
