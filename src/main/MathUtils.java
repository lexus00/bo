package main;

public class MathUtils {
    // m!
    public static long factorial(int m) {
        long result = 1;

        for(int i = 2; i <= m; i ++) {
            result *= i;
        }

        return result;
    }

    // sum of series x^i/i! from i = m to n
    public static double sum_of_series(int m, int n, double v) {
        double sum = 0;
        for(int i = m; i <= n; i++) {
            sum += (Math.pow(v, i)/factorial(i));
        }
        return sum;
    }
}
